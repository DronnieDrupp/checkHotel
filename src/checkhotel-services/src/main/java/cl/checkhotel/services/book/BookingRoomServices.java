package cl.checkhotel.services.book;

import cl.checkhotel.domain.entity.bussines.BookingRoomEntity;

public interface BookingRoomServices {
	
	BookingRoomEntity roomByBooking(Long idBooking);

}
