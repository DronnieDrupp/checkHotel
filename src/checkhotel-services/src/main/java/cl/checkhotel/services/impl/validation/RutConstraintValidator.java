package cl.checkhotel.services.impl.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cl.checkhotel.domain.base.validation.constraints.Rut;
import cl.checkhotel.domain.helper.RutHelper;

public class RutConstraintValidator implements
		ConstraintValidator<Rut, String> {

	@Override
	public void initialize(Rut constraintAnnotation) {
		// Sin implementación
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (null == value) {
			return true;
		}
		return RutHelper.isValid(value);
	}
}
