package cl.checkhotel.services.impl.adm;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.UserRepository;
import cl.checkhotel.domain.helper.ValidationHelper;
import cl.checkhotel.domain.helper.ValidatorHelper;
import cl.checkhotel.domain.user.User;
import cl.checkhotel.exceptions.ObjectNotFoundException;
import cl.checkhotel.exceptions.PasswordEncodeException;
import cl.checkhotel.services.adm.UserService;

@Service
@Transactional
public class UserServiceImpl extends AbstractCrudService<User> implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private Validator validator;

	@Override
	@Transactional(readOnly = true)
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public void save(User entity) {
		ValidatorHelper.validateIllegalArgument(entity, "user");

		ValidationHelper.throwExceptionIfObjectIsNotValid(validator, entity);

		entity.setPassword(encryptPassword(entity.getPassword()));
		// entity.setEnabled("true");
		userRepository.save(entity);
	}

	@Override
	@Transactional(readOnly = true)
	public void update(User entity) {
		ValidatorHelper.validateIllegalArgument(entity, "user");

		ValidationHelper.throwExceptionIfObjectIsNotValid(validator, entity);

		User user = userRepository.findOne(entity.getId());

		if (null == user) {
			throw new ObjectNotFoundException("");
		}

		user.setName(entity.getName());
		user.setPassword(encryptPassword(entity.getPassword()));
		user.setRole(entity.getRole());
		// user.setEnabled(entity.getEnabled());
		userRepository.save(entity);
	}

	private static String encryptPassword(String password) {

		MessageDigest crypt = null;
		try {
			crypt = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			throw new PasswordEncodeException();
		}
		crypt.reset();
		try {
			crypt.update(password.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			throw new PasswordEncodeException();
		}

		return new BigInteger(1, crypt.digest()).toString(16);
	}

	@Override
	public void delete(Long id) {
		ValidatorHelper.validateIllegalArgument(id, "userId");
		userRepository.delete(id);

	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {
		ValidatorHelper.validateIllegalArgument(id, "userId");
		return userRepository.findOne(id);
	}

	private Sort sortByNameASC() {
		return new Sort(Sort.Direction.DESC, "name");
	}

	private Page<User> executeQueryFindByName(int page, int maxResults, String name) {
		final PageRequest pageRequest = new PageRequest(page, maxResults, sortByNameASC());

		return userRepository.findByNameLike(pageRequest, "%" + name + "%");
	}

	@Override
	@Transactional(readOnly = true)
	public Page<User> findByNameLike(int page, int maxResults, String name) {
		Page<User> result = executeQueryFindByName(page, maxResults, name);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindByName(lastPage, maxResults, name);
		}

		return result;
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<User> findAll(Pageable pageable) {
		return userRepository.findAll(pageable);
	}
}
