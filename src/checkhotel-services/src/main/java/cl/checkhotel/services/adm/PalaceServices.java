package cl.checkhotel.services.adm;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;

public interface PalaceServices {
	
	void savePalace(PalaceEntity palace);
	
	Page<PalaceEntity> listPalaceEntities(Long idClient, Pageable  pageable);
	
	PalaceEntity palaceById(Long idPalace);
	
	void deletePalace(Long idPalace);
	
	void updatePalace(PalaceEntity palace);
	

}
