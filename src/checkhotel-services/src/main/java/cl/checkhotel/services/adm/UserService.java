package cl.checkhotel.services.adm;

import org.springframework.data.domain.Page;

import cl.checkhotel.domain.user.User;

public interface UserService extends CrudService<User> {

	User findByEmail(String email);

	Page<User> findByNameLike(int page, int maxResults, String name);
}
