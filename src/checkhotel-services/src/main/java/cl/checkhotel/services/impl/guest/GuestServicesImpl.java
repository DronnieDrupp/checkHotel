package cl.checkhotel.services.impl.guest;

import java.util.Collection;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.guest.GuestRepository;
import cl.checkhotel.domain.entity.bussines.GuestEntity;
import cl.checkhotel.domain.helper.ValidationHelper;
import cl.checkhotel.exceptions.ObjectNotFoundException;
import cl.checkhotel.services.guest.GuestService;

@Service("guestService")
@Transactional
public class GuestServicesImpl implements GuestService {

	@Autowired
	private GuestRepository guestRepository;

	@Autowired
	private Validator validator;

	@Override
	public void saveGuest(GuestEntity guest) {
		guest.setActive(true);

		validator.validate(guest);
		try {
			guestRepository.saveAndFlush(guest);
		} catch (Exception ex) {

			throw new ObjectNotFoundException(
					"cl.checkhotel.domain.entity.bussines.GuestEntity.notFound.message");

		}
	}

	@Override
	@Transactional(readOnly=true)
	public Collection<GuestEntity> listGuestByClient(Integer idCLient) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Transactional(readOnly=true)
	public GuestEntity guestById(Long idGuest) {

		return guestRepository.findOne(idGuest);
	}

	@Override
	@Transactional(readOnly=true)
	public GuestEntity guestByIdLegal(String idLegalGuest) {

		return guestRepository.findByIdLegal(idLegalGuest);
	}

	@Override
	public void deleteGuest(Integer idGuest) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateGuest(GuestEntity guestEntity) {
		if (null == guestEntity) {
			throw new IllegalArgumentException("El huesped no puede ser nulo");
		}

		GuestEntity existantGuestEntity = guestRepository.getOne(guestEntity
				.getId());

		if (null == existantGuestEntity) {
			throw new ObjectNotFoundException(
					"cl.checkhotel.domain.entity.bussines.GuestEntity.notFound.message");
		}

		existantGuestEntity.setEmail(guestEntity.getEmail());
		existantGuestEntity.setFirstName(guestEntity.getFirstName());
		existantGuestEntity.setLastName(guestEntity.getLastName());
		existantGuestEntity.setLegalId(guestEntity.getLegalId());

		ValidationHelper.throwExceptionIfObjectIsNotValid(validator,
				existantGuestEntity);

		guestRepository.saveAndFlush(existantGuestEntity);
	}

}
