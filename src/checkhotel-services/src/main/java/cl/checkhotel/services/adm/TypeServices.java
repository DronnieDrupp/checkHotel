package cl.checkhotel.services.adm;
import java.util.Collection;

import cl.checkhotel.domain.entity.bussines.TypeEntity;

public interface TypeServices {

	Collection<TypeEntity> findByIndicator(Integer indicator);
}
