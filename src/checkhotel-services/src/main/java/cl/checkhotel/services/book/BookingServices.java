package cl.checkhotel.services.book;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.checkhotel.domain.entity.bussines.BookingEntity;

public interface BookingServices {

	/**
	 * Cancela o elimina logicamente una reserva
	 * 
	 * @param idBooking
	 */
	void cancelBooking(Long idBooking);

	/**
	 * Actualiza una reserva
	 * 
	 * @param booking
	 */
	void updateBooking(BookingEntity booking);

	/**
	 * Crea una reserva para una habitacion
	 * 
	 * @param booking
	 */
	void createBookings(BookingEntity booking);

	/**
	 * Ejecuta el pago de una reserva
	 * 
	 * @param idBooking
	 */
	void payBooking(Long idBooking);

	/**
	 * Obtiene el listado de reservas asociadas a un cliente
	 * @param idClient
	 * @return
	 */
	public Page<BookingEntity> listBookins(Long idCliente, Pageable pageable);

	/**
	 * Localiza una reserva por el identificador de reserva
	 * @param idBooking
	 * @return
	 */
	BookingEntity searchIdBooking(Long idBooking);

}
