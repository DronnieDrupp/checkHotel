package cl.checkhotel.services.impl.adm;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.TypeRepository;
import cl.checkhotel.domain.entity.bussines.TypeEntity;
import cl.checkhotel.services.adm.TypeServices;

@Service("typeServices")
public class TypeServicesImpl implements TypeServices {
	
	@Autowired
	private TypeRepository typeRepository;

	@Override
	@Transactional(readOnly=true)
	public Collection<TypeEntity> findByIndicator(Integer indicator) {

		return typeRepository.findByIndicator(indicator);
		
	}

}
