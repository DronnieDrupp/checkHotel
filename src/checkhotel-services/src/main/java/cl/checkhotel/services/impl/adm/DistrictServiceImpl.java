package cl.checkhotel.services.impl.adm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.DistrictRepository;
import cl.checkhotel.domain.entity.bussines.DistrictEntity;
import cl.checkhotel.services.adm.DistrictServices;

@Service("communeServices")
public class DistrictServiceImpl implements DistrictServices {

	@Autowired
	private DistrictRepository districtRepository;
	
	@Override
	@Transactional(readOnly=true)
	public List<DistrictEntity> listAll() {
		
		return districtRepository.findAll();
	}

}
