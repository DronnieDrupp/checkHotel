package cl.checkhotel.services.impl.validation;

import java.util.Collection;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.CollectionUtils;

import cl.checkhotel.dao.jpa.book.BookingRepository;
import cl.checkhotel.domain.base.validation.constraints.ReservationAvailability;
import cl.checkhotel.domain.entity.bussines.BookingEntity;

public class ReservationAvailabilityValidator implements ConstraintValidator<ReservationAvailability, BookingEntity> {

	BookingRepository bookingRepository;

	public void setBookingRepository(BookingRepository bookingRepository) {
		this.bookingRepository = bookingRepository;
	}

	@Override
	public void initialize(ReservationAvailability constraintAnnotation) {
	}

	@Override
	public boolean isValid(BookingEntity value, ConstraintValidatorContext context) {
		if (null == value){
			return Boolean.TRUE;
		}
		if (null == value.getDateFrom()){
			return Boolean.TRUE;
		}
		if (null == value.getDateTo()){
			return Boolean.TRUE;
		}

		//buscamos las reservas de la habitacion en el rango de fecha solicitado y que no sean la reserva que estoy agregando
		Collection<BookingEntity> listOfBookings = bookingRepository.findBookingsBeetwenDates(value.getDateFrom(),
				value.getDateTo(), value.getId(), value.getRoomEntity().getId());

		return CollectionUtils.isEmpty(listOfBookings);
	}

}
