package cl.checkhotel.services.impl.book;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.book.BookingRoomRepository;
import cl.checkhotel.domain.entity.bussines.BookingRoomEntity;
import cl.checkhotel.services.book.BookingRoomServices;

@Service("bookingRoomServices")
@Transactional
public class BookingRoomServicesImpl implements BookingRoomServices {

	@Autowired
	BookingRoomRepository bookingRoomRepository;

	@Override
	@Transactional(readOnly = true)
	public BookingRoomEntity roomByBooking(Long idBooking) {
		// TODO Auto-generated method stub
		return bookingRoomRepository.findOne(idBooking);
	}

}
