package cl.checkhotel.services.impl.pay;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import cl.checkhotel.dao.jpa.book.BookingRepository;
import cl.checkhotel.dao.jpa.book.PayRepository;
import cl.checkhotel.domain.entity.bussines.PayEntity;
import cl.checkhotel.services.pay.PayService;

@Service("payService")
@Transactional
public class PayServiceImpl implements PayService {
	
	@Autowired
	private PayRepository payRepository;
	
	@Autowired
	private BookingRepository bookingRepository;

	@Override
	public void saveOrUpdate(PayEntity pay) {
		
		PayEntity persistentPay = searchPayByBooking(pay.getBooking());
		
		if(null == persistentPay){
			pay.setActive(Boolean.TRUE);
			payRepository.save(pay);
		}else{
			try{
			payRepository.saveAndFlush(updatePay(persistentPay,pay));
			}
			catch (Exception e) {
				String ex = e.getMessage();
			}
		}
		
		

	}
	
	private PayEntity updatePay(PayEntity persistentPay, PayEntity pay){
		
		persistentPay.setNumerdCard(pay.getNumerdCard());
		persistentPay.setPaymentType(pay.getPaymentType()); 
		persistentPay.setVoucher(StringUtils.isEmpty(pay.getVoucher()) ? null : pay.getVoucher());
		
		return persistentPay;
	}

	@Override
	public Collection<PayEntity> listPayEntitys(Integer idCliente) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<PayEntity> listPayEntitys(Integer idCliente,
			Integer idBooking) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PayEntity searchPayByBooking(Long bookingId) {
		return payRepository.findByBooking(bookingId);
	}

}
