package cl.checkhotel.services.authentication;

import cl.checkhotel.domain.user.User;

/**
 * Define la forma de obtener el usuario actualmente logeado
 *
 * $Id$
 * $Revision$
 * 
 * @author ftroncoso
 *
 */
public interface SessionManager {

	/**
	 * Retorna el usuario actualmente logeado
	 * 
	 * @return
	 */
	User getCurrentUser();
}
