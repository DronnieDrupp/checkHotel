package cl.checkhotel.services.adm;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.RoomEntity;

public interface RoomServices {

	void saveRoom(RoomEntity room);

	Page<RoomEntity> listRoomEntities(Long idPalace, Pageable  pageable);
	
	Page<RoomEntity> listRoomForBooking(Long idCliente, BookingEntity bookingEntity, Pageable  pageable);

	RoomEntity roomById(Long idRoom);

	void deleteRoom(Long idRoom);
	
	void deleteRoomByPalaceId(Long palaceId);

	void updateRoom(RoomEntity room);

}
