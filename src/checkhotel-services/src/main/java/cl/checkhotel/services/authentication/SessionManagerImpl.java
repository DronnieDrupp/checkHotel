package cl.checkhotel.services.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import cl.checkhotel.domain.user.User;
import cl.checkhotel.services.adm.UserService;

/**
 *
 * $Id$
 * $Revision$
 * 
 * @author ftroncoso
 *
 */
@Service
public class SessionManagerImpl implements SessionManager {
	
	@Autowired
	private UserService userService;

	@Override
	public User getCurrentUser() {
		SecurityContext context = SecurityContextHolder.getContext();

		if (context == null)
			return null;

		Authentication authentication = context.getAuthentication();

		if (authentication == null || !authentication.isAuthenticated())
			return null;

		Object principal = authentication.getPrincipal();

		if (principal == null)
			return null;

		String email = authentication.getName();
		User user = userService.findByEmail(email);
		return user;
	}

}
