package cl.checkhotel.services.impl.adm;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.RoomRepository;
import cl.checkhotel.dao.jpa.book.BookingRepository;
import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.services.adm.RoomServices;

@Service("roomServices")
@Transactional
public class RoomServicesImpl implements RoomServices {

	@Autowired
	private RoomRepository roomRepository;

	private int maxResults;

	@Autowired
	private BookingRepository bookingRepository;

	@Override
	public void saveRoom(RoomEntity room) {

		room.setActive(Boolean.TRUE);

		roomRepository.save(room);

	}

	@Override
	@Transactional(readOnly=true)
	public Page<RoomEntity> listRoomEntities(Long palaceId, Pageable pageable) {

		Page<RoomEntity> listRooms = null;

		listRooms = roomRepository.findByPalace(pageable, palaceId);

		return listRooms;
	}

	@Override
	@Transactional(readOnly=true)
	public RoomEntity roomById(Long idRoom) {

		return roomRepository.findOne(idRoom);

	}

	@Override
	public void deleteRoom(Long idRoom) {
		RoomEntity room = roomById(idRoom);
		room.setActive(Boolean.FALSE);
		roomRepository.save(room);

	}

	@Override
	public void updateRoom(RoomEntity room) {

		RoomEntity persistedRoom = roomById(room.getId());

		roomRepository.save(mergeRoom(room, persistedRoom));

	}

	/**
	 * 
	 * @param roomView
	 *            , Habitación extraida de la vista
	 * @param persistedRoom
	 *            , Habitación que se encuentra previamente persistida y se
	 *            actualizara
	 * @return
	 */
	private RoomEntity mergeRoom(RoomEntity roomView, RoomEntity persistedRoom) {

		persistedRoom.setActive(Boolean.TRUE);
		persistedRoom.setAddress(roomView.getAddress());
		persistedRoom.setComment(roomView.getComment());
		persistedRoom.setDescription(roomView.getDescription());
		persistedRoom.setLink(roomView.getLink());
		persistedRoom.setNumberPerson(roomView.getNumberPerson());
		persistedRoom.setPrice(roomView.getPrice());
		persistedRoom.setProperties(roomView.getProperties());
		persistedRoom.setType(roomView.getType());

		return persistedRoom;
	}

	@Override
	public void deleteRoomByPalaceId(Long palaceId) {

		for (RoomEntity room : roomRepository.findByPalace(null, palaceId)) {
			room.setActive(Boolean.FALSE);
			roomRepository.save(room);
		}

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@Transactional(readOnly=true)
	public Page<RoomEntity> listRoomForBooking(Long idCliente,
			BookingEntity bookingEntity, Pageable pageable) {

		Page<RoomEntity> combinedList = roomRepository.findByClientAndNumberPerson(pageable,
				idCliente, bookingEntity.getNumberSearch());
		
		List<RoomEntity> rooms = new ArrayList<>();
		
		for(RoomEntity room : combinedList.getContent()){
			if(bookingRepository.findBookingsBeetwenDates(bookingEntity.getDateFrom(), bookingEntity.getDateTo(), room.getId()).isEmpty()){
				rooms.add(room);
			}
			
		}
		return new PageImpl(rooms, pageable, rooms.size());
		
	}

}
