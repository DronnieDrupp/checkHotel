package cl.checkhotel.services.impl.validation;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import cl.checkhotel.domain.base.validation.constraints.Range;
import cl.checkhotel.domain.entity.bussines.BookingEntity;

public class BookingEntityRangeDatesConstraintValidator implements
		ConstraintValidator<Range, BookingEntity> {

	private static final String MESSAGE_TO_RANGE_TEMPLATE = "{cl.checkhotel.domain.base.validation.constraints.Range.cl.checkhotel.domain.entity.bussines.BookingEntity.dateTo.message}";

	private static final String DATE_TO = "dateTo";

	@Override
	public boolean isValid(BookingEntity value,
			ConstraintValidatorContext context) {
		if (null == value) {
			return Boolean.TRUE;
		}
		Date from = value.getDateFrom();
		Date to = value.getDateTo();

		if (from != null && to != null && !to.after(from)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(
					MESSAGE_TO_RANGE_TEMPLATE).addNode(DATE_TO)
					.addConstraintViolation();
			return Boolean.FALSE;
		}

		return Boolean.TRUE;
	}

	@Override
	public void initialize(Range constraintAnnotation) {
	}

}
