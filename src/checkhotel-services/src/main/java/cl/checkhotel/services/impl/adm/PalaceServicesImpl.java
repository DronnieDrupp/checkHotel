package cl.checkhotel.services.impl.adm;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.PalaceRepository;
import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.domain.helper.ValidationHelper;
import cl.checkhotel.exceptions.ObjectNotFoundException;
import cl.checkhotel.services.adm.PalaceServices;
import cl.checkhotel.services.adm.RoomServices;

@Service("palaceServices")
@Transactional
public class PalaceServicesImpl implements PalaceServices {

	@Autowired
	private PalaceRepository palaceRepository;
	
	@Autowired
	private RoomServices roomServices;
	
	@Autowired
	private Validator validator;

	@Override
	public void savePalace(PalaceEntity palace) {
		
		if (null==palace){
			throw new IllegalArgumentException("palace no puede ser nulo");
		}
		palace.setActive(Boolean.TRUE);
		
		ValidationHelper.throwExceptionIfObjectIsNotValid(validator, palace);
		
		palaceRepository.save(palace);

	}

	@Override
	@Transactional(readOnly=true)
	public Page<PalaceEntity> listPalaceEntities(Long idClient, Pageable  pageable) {

		if (null==idClient){
			throw new IllegalArgumentException("idClient no puede ser nulo");
		}

		Page<PalaceEntity> listPalaces = null;

		listPalaces = palaceRepository.findAllByClient(pageable, idClient);

		if (null == listPalaces) {
			throw new ObjectNotFoundException(
					"cl.checkhotel.domain.entity.bussines.PalaceEntity.notFound.message");
		}

		return listPalaces;

	}

	@Override
	@Transactional(readOnly=true)
	public PalaceEntity palaceById(Long idPalace) {

		if (null==idPalace){
			throw new IllegalArgumentException("idClient no puede ser nulo");
		}
		
		PalaceEntity palace = palaceRepository.findOne(idPalace);

		if (null == palace) {
			throw new ObjectNotFoundException(
					"cl.checkhotel.domain.entity.bussines.PalaceEntity.notFound.message");
		}

		
		return palace;

	}

	@Override
	public void deletePalace(Long idPalace) {

		if (null==idPalace){
			throw new IllegalArgumentException("idClient no puede ser nulo");
		}

		PalaceEntity palace = palaceRepository.findOne(idPalace);
		
		if (null == palace) {
			throw new ObjectNotFoundException(
					"cl.checkhotel.domain.entity.bussines.PalaceEntity.notFound.message");
		}
		
		palace.setActive(Boolean.FALSE);
		palaceRepository.save(palace);
		roomServices.deleteRoomByPalaceId(idPalace);
	}

	@Override
	public void updatePalace(PalaceEntity palace) {
		
		if (null==palace){
			throw new IllegalArgumentException("v no puede ser nulo");
		}

		ValidationHelper.throwExceptionIfObjectIsNotValid(validator, palace);
		
		PalaceEntity oldPalace = palaceRepository.findOne(palace.getId());
		palace.setActive(oldPalace.getActive());
		palace.setClient(oldPalace.getClient());
		palaceRepository.save(palace);

	}
}
