package cl.checkhotel.services.adm;

import java.util.List;

import cl.checkhotel.domain.entity.bussines.DistrictEntity;

public interface DistrictServices {
	
	List<DistrictEntity> listAll();
	
}
