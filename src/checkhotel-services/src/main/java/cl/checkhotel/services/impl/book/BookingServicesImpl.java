package cl.checkhotel.services.impl.book;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.RoomRepository;
import cl.checkhotel.dao.jpa.book.BookingRepository;
import cl.checkhotel.dao.jpa.book.BookingRoomRepository;
import cl.checkhotel.domain.basicEnum.Status;
import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.GuestEntity;
import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.domain.helper.ValidationHelper;
import cl.checkhotel.exceptions.ObjectNotFoundException;
import cl.checkhotel.services.book.BookingServices;
import cl.checkhotel.services.guest.GuestService;

@Service("bookingServices")
@Transactional
public class BookingServicesImpl implements BookingServices {

	@Autowired
	private BookingRepository bookingRepository;

	@Autowired
	private BookingRoomRepository bookingRoomRepository;
	
	@Autowired
	private RoomRepository roomRepository;

	@Autowired
	private GuestService guestServices;

	@Autowired
	private Validator validator;

	@Override
	public void cancelBooking(Long idBooking) {
		if (null == idBooking) {
			throw new IllegalArgumentException("El id de la reserva no puede ser nulo");
		}
		BookingEntity booking = bookingRepository.getOne(idBooking);

		if (null == booking) {
			throw new ObjectNotFoundException("cl.checkhotel.domain.entity.bussines.BookingEntity.notFound.message");
		}
		booking.setActive(Boolean.FALSE);
		bookingRepository.saveAndFlush(booking);
	}

	@Override
	public void createBookings(BookingEntity booking) {
		if (null == booking) {
			throw new IllegalArgumentException("La reserva no puede ser nulo");
		}
		
		booking.setActive(Boolean.TRUE);
		booking.setStatus(Status.ACTIVE.getDesc());
		
		ValidationHelper.throwExceptionIfObjectIsNotValid(validator, booking);

		saveGuest(booking.getGuestEntity());
		
		bookingRepository.saveAndFlush(booking);

		// aqui deberias invocar al servicio de roomsBooking por el patron de
		// separacion de responsabilidades
		//bookingRoomRepository.save(createIterable(booking));

	}

	private void saveGuest(GuestEntity guestEntity) {
		GuestEntity persistentGuest = guestServices.guestByIdLegal(guestEntity.getLegalId());
		
		if(null == persistentGuest){
			guestServices.saveGuest(guestEntity);
		}else {
			
			guestEntity.setId(persistentGuest.getId());
			
			guestServices.updateGuest(guestEntity);
		}
		
	}

	@Override
	public void payBooking(Long idBooking) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional(readOnly = true)
	public Page<BookingEntity> listBookins(Long idCliente, Pageable pageable) {
		
		try {
			return bookingRepository.findAll(pageable);
		} catch (Exception e) {
			return null;
		}
		
	}

	@Override
	@Transactional(readOnly = true)
	public BookingEntity searchIdBooking(Long idBooking) {
		if (null == idBooking) {
			throw new IllegalArgumentException("El id de la reserva no puede ser nulo");
		}

		BookingEntity newBooking = bookingRepository.findOne(idBooking);

		if (null == newBooking) {
			throw new ObjectNotFoundException("cl.checkhotel.domain.entity.bussines.BookingEntity.notFound.message");
		}
		

		return newBooking;
	}

	@Override
	public void updateBooking(BookingEntity booking) {
		if (null == booking) {
			throw new IllegalArgumentException("La reserva no puede ser nulo");
		}
		
		BookingEntity existantBooking = bookingRepository.findOne(booking.getId());
		
		if (null == existantBooking) {
			throw new ObjectNotFoundException("cl.checkhotel.domain.entity.bussines.BookingEntity.notFound.message");
		}
		
		RoomEntity roomEntity = roomRepository.findOne(booking.getRoomEntity().getId());
		
		if (null == roomEntity) {
			throw new ObjectNotFoundException("cl.checkhotel.domain.entity.bussines.RoomEntity.notFound.message");
		}
		
		existantBooking.setActive(booking.getActive());
		existantBooking.setStatus(booking.getStatus());
		existantBooking.setRoomEntity(roomEntity);
		existantBooking.setDateFrom(booking.getDateFrom());
		existantBooking.setDateTo(booking.getDateTo());
		
		guestServices.updateGuest(booking.getGuestEntity());
		
		existantBooking.getGuestEntity().setId(booking.getGuestEntity().getId());
		GuestEntity guestUpdated = guestServices.guestById(booking.getGuestEntity().getId());
		booking.setGuestEntity(guestUpdated);
		
		//ValidationHelper.throwExceptionIfObjectIsNotValid(validator, existantBooking);


		
		
		bookingRepository.saveAndFlush(existantBooking);

	}

}
