package cl.checkhotel.services.impl.adm;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.dao.jpa.adm.CountryRepository;
import cl.checkhotel.domain.entity.bussines.CountryEntity;
import cl.checkhotel.services.adm.CountryServices;

@Service("countryServices")
public class CountryServicesImpl implements CountryServices {
	
	@Autowired
	private CountryRepository countryRepository;

	@Override
	@Transactional(readOnly=true)
	public List<CountryEntity> listAll() {
		return countryRepository.findAll();
	}

}
