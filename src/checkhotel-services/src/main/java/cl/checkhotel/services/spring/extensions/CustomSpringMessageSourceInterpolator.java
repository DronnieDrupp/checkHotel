package cl.checkhotel.services.spring.extensions;

import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.MessageInterpolator;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.NoSuchMessageException;

public class CustomSpringMessageSourceInterpolator extends
		ResourceBundleMessageInterpolator implements MessageInterpolator,
		MessageSourceAware, InitializingBean {

	private static final Pattern MESSAGE_PARAMETER_PATTERN = Pattern
			.compile("(\\{[^\\}]+?\\})");

	private MessageSource messageSource;

	@Override
	public String interpolate(String messageTemplate, Context context) {
		try {
			String messageTemplateSpring = removeFirstAndLast(messageTemplate);
			return replaceAnnotationAttributes(
					messageSource.getMessage(messageTemplateSpring,
							new Object[] {}, Locale.getDefault()), context
							.getConstraintDescriptor().getAttributes());
		} catch (NoSuchMessageException e) {
			return super.interpolate(messageTemplate, context);
		}
	}

	@Override
	public String interpolate(String messageTemplate, Context context,
			Locale locale) {
		try {

			String messageTemplateSpring = removeFirstAndLast(messageTemplate);
			return replaceAnnotationAttributes(messageSource.getMessage(
					messageTemplateSpring, new Object[] {}, locale), context
					.getConstraintDescriptor().getAttributes());
		} catch (NoSuchMessageException e) {
			return super.interpolate(messageTemplate, context, locale);
		}
	}

	private String replaceAnnotationAttributes(String message,
			Map<String, Object> annotationParameters) {
		Matcher matcher = MESSAGE_PARAMETER_PATTERN.matcher(message);
		StringBuffer sb = new StringBuffer();
		while (matcher.find()) {
			String resolvedParameterValue;
			String parameter = matcher.group(1);
			Object variable = annotationParameters
					.get(removeFirstAndLast(parameter));
			if (variable != null) {
				if (variable.getClass().isArray()) {
					resolvedParameterValue = Arrays
							.toString((Object[]) variable);
				} else {
					resolvedParameterValue = variable.toString();
				}
			} else {
				resolvedParameterValue = parameter;
			}
			resolvedParameterValue = Matcher
					.quoteReplacement(resolvedParameterValue);
			matcher.appendReplacement(sb, resolvedParameterValue);
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		if (messageSource == null) {
			throw new IllegalStateException(
					"MessageSource was not injected, could not initialize "
							+ this.getClass().getSimpleName());
		}
	}

	private static String removeFirstAndLast(String string) {
		return string.substring(1, string.length() - 1);
	}

}