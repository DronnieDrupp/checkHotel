package cl.checkhotel.services.adm;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CrudService<T> {

	Page<T> findAll(Pageable pageable);

	Collection<T> findAll();

	void save(T entity);
	
	void update(T entity);

	void delete(Long id);
	
	T findById(Long id);

}
