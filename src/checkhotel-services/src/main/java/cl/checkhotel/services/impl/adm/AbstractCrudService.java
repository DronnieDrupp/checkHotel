package cl.checkhotel.services.impl.adm;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import cl.checkhotel.services.adm.CrudService;

public abstract class AbstractCrudService<T> implements CrudService<T> {

	protected boolean shouldExecuteSameQueryInLastPage(int page, Page<T> result) {
		return isUserAfterOrOnLastPage(page, result)
				&& hasDataInDataBase(result);
	}

	private boolean isUserAfterOrOnLastPage(int page, Page<T> result) {
		return page >= result.getTotalPages() - 1;
	}

	private boolean hasDataInDataBase(Page<T> result) {
		return result.getTotalElements() > 0;
	}

}
