package cl.checkhotel.services.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorFactory;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class CustomConstraintValidatorFactory implements
		ConstraintValidatorFactory, ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Override
	public <T extends ConstraintValidator<?, ?>> T getInstance(Class<T> key) {
		try {
			return this.applicationContext.getBean(key);
		} catch (NoSuchBeanDefinitionException e) {
			return this.applicationContext.getAutowireCapableBeanFactory()
					.createBean(key);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)
			throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public void releaseInstance(ConstraintValidator<?, ?> instance) {
		// TODO Auto-generated method stub
	}

}
