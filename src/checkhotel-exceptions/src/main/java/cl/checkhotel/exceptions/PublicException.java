package cl.checkhotel.exceptions;

import java.util.Locale;

/**
 * Base para exceptions que se consideran públicas. Esto es, aquellas cuya
 * descripción puede ser transparentada al usuario.
 * 
 */
public class PublicException extends LocalizedException {

	private static final long serialVersionUID = 1L;

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, Object[] args, String defaultMessage,
			Locale locale, Throwable cause) {
		super(code, args, defaultMessage, locale, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, String defaultMessage) {
		super(code, defaultMessage);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, String defaultMessage, Locale locale) {
		super(code, defaultMessage, locale);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param message
	 *            El mensaje que describe la exception, ya internacionalizado.
	 */
	public PublicException(String message) {
		super(message);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, Object[] args, String defaultMessage,
			Locale locale) {
		super(code, args, defaultMessage, locale);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, String defaultMessage, Throwable cause) {
		super(code, defaultMessage, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, String defaultMessage, Locale locale,
			Throwable cause) {
		super(code, defaultMessage, locale, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param message
	 *            El mensaje que describe la exception.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, Object[] args, String defaultMessage) {
		super(code, args, defaultMessage);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public PublicException(String code, Object[] args, String defaultMessage,
			Throwable cause) {
		super(code, args, defaultMessage, cause);
	}

}
