package cl.checkhotel.exceptions;

/**
 * Se gatilla cuando no se encuenta un objeto.
 */
public class ObjectNotFoundException extends LocalizedException {

	/**
	 * Inicializa la instancia con los valores especificados.
	 */
	public ObjectNotFoundException(Class<?> clazz, Object id,
			String defaultMessage) {
		super("cl.checkhotel.exceptions.ObjectNotFoundException."
				+ clazz.getCanonicalName(), new Object[] { id }, defaultMessage);
	}

        /**
         * Inicializa la instancia con los valores especificados.
         * 
         * @param message 
         */
        public ObjectNotFoundException(String message) {
		super(message);
	}

	public ObjectNotFoundException(String code, String defaultMessage) {
		super(code, defaultMessage);
	}

	private static final long serialVersionUID = 1L;

}
