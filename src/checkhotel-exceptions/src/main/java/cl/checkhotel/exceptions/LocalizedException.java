package cl.checkhotel.exceptions;

//import cl.taisachile.antaios.messages.MessageSource;
//import cl.taisachile.antaios.messages.MessageSourceFactory;
import java.util.Locale;

import cl.checkhotel.messages.MessageSource;
import cl.checkhotel.messages.MessageSourceFactory;

/**
 * Base para la creación de exceptions con soporte para internacionalización.
 */
public class LocalizedException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private String localizedMessage;

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, Object[] args, String defaultMessage, Locale locale, Throwable cause) {
		super(cause);
		initialize(code, args, defaultMessage, locale);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, String defaultMessage) {
		this(code, (Object[]) null, defaultMessage, (Locale) null);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, String defaultMessage, Locale locale) {
		this(code, (Object[]) null, defaultMessage, locale);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param message
	 *            El mensaje que describe la exception, ya internacionalizado.
	 */
	public LocalizedException(String message) {
		this((String) null, (Object[]) null, message, (Locale) null);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, Object[] args, String defaultMessage, Locale locale) {
		initialize(code, args, defaultMessage, locale);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, String defaultMessage, Throwable cause) {
		this(code, (String[]) null, defaultMessage, (Locale) null, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param locale
	 *            El locale que identifica la fuente de mensajes a usar para
	 *            resolver internacionalización.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, String defaultMessage, Locale locale, Throwable cause) {
		this(code, (Object[]) null, defaultMessage, locale, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param message
	 *            El mensaje que describe la exception.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String message, Throwable cause) {
		this((String) null, (Object[]) null, message, (Locale) null, cause);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, Object[] args, String defaultMessage) {
		this(code, args, defaultMessage, (Locale) null);
	}

	/**
	 * Inicializa la instancia con los valores especificados.
	 *
	 * @param code
	 *            El código de internacionalización del mensaje. Se recomienda
	 *            usar el fullname de la clase como prefijo para evitar
	 *            conflictos de nombres. Ejemplo:
	 *            "com.kleuche.users.exceptions.validationException.invalidDNI"
	 * @param args
	 *            Un arreglo de objetos a usar como argumentos para la
	 *            resolución de mensajes. Para hacer referencia a los
	 *            argumentos, los mensajes usan cadenas tales como {0},
	 *            {1,date}, {2,time}
	 * @param defaultMessage
	 *            El mensaje por defecto que aplica si el código de
	 *            internacionalización no existe.
	 * @param cause
	 *            La exception que origina esta instancia.
	 * @see java.text.MessageFormat
	 */
	public LocalizedException(String code, Object[] args, String defaultMessage, Throwable cause) {
		this(code, args, defaultMessage, (Locale) null, cause);
	}

	private void initialize(String code, Object[] args, String defaultMessage, Locale locale) {

		if (args != null) {
			defaultMessage = String.format(defaultMessage, args);
		}

		if (code == null) {
			this.localizedMessage = defaultMessage;
		} else {
			// this.localizedMessage = resolveMessage(code, args,
			// defaultMessage, locale);
		}
	}

	@Override
	public String getMessage() {
		return getLocalizedMessage();
	}

	@Override
	public String getLocalizedMessage() {
		return this.localizedMessage;
	}

	/**
	 * Resuelve el mensaje con los datos especificados.
	 */
	protected String resolveMessage(String code, Object[] args, String defaultMessage, Locale locale) {
		MessageSource msgSource = getMessageSource();

		if (msgSource == null) {
			return defaultMessage;
		}

		return msgSource.getMessage(code, args, defaultMessage, locale);
	}

	/**
	 * Obtiene la fuente de mensajes.
	 */
	protected MessageSource getMessageSource() {
		MessageSourceFactory factory = MessageSourceFactory.getFactory();

		return factory == null ? null : factory.getMessageSource();
	}
}
