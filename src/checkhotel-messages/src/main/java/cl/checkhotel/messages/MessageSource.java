package cl.checkhotel.messages;

import java.util.Locale;

/**
 * Interface para clases que implementan fuentes de mensajes.
 */
public interface MessageSource {

	/**
	 * Resuelve un mensaje con los valores especificados.
	 * 
	 * @param code
	 *            El código de internacionalización del mensaje.
	 * @param args
	 *            Los argumentos usados para resolver los placeholder en el
	 *            mensaje.
	 * @param defaultMessage
	 *            El mensaje a usar si el código no existe.
	 * @param locale
	 *            El locale usado para identificar la fuente de los mensajes.
	 * @return El mensaje resuelto o defaultMessage si code no existe.
	 */
	String getMessage(String code, Object[] args, String defaultMessage,
			Locale locale);

}
