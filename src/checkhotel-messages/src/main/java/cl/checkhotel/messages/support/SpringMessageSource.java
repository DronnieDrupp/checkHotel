package cl.checkhotel.messages.support;

import java.util.Locale;

import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;

import cl.checkhotel.messages.MessageSource;

/**
 * Implementación por defecto de MessageSource.
 */
public class SpringMessageSource implements MessageSource,
		org.springframework.context.MessageSource {

	private final org.springframework.context.MessageSource messageSource;

	/**
	 * Inicializa la instancia con la fuente de mensajes especificada.
	 * 
	 * @param messageSource
	 *            La fuente de mensajes.
	 */
	public SpringMessageSource(
			org.springframework.context.MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	@Override
	public String getMessage(String code, Object[] args, String defaultMessage,
			Locale locale) {
		return messageSource.getMessage(code, args, defaultMessage, locale);
	}

	@Override
	public String getMessage(String code, Object[] args, Locale locale)
			throws NoSuchMessageException {
		return messageSource.getMessage(code, args, locale);
	}

	@Override
	public String getMessage(MessageSourceResolvable resolvable, Locale locale)
			throws NoSuchMessageException {
		return messageSource.getMessage(resolvable, locale);
	}

}
