package cl.checkhotel.messages.support;

import java.util.Locale;

import org.springframework.context.MessageSourceAware;

import cl.checkhotel.messages.MessageSource;
import cl.checkhotel.messages.MessageSourceFactory;

/**
 * Implementación de fábrica de fuentes de mensajes basada en Spring.
 */
public class SpringMessageSourceFactory extends MessageSourceFactory implements
		MessageSourceAware, MessageSource {

	private SpringMessageSource messageSource;

	@Override
	public MessageSource getMessageSource() {
		return this.messageSource;
	}

	@Override
	public void setMessageSource(
			org.springframework.context.MessageSource messageSource) {
		this.messageSource = new SpringMessageSource(messageSource);
	}

	/**
	 * Inicializa la instancia.
	 */
	public void initialize() {
		MessageSourceFactory.setFactory(this);
	}

	@Override
	public String getMessage(String code, Object[] args, String defaultMessage,
			Locale locale) {
		return messageSource.getMessage(code, args, defaultMessage, locale);
	}

}
