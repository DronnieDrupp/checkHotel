package cl.checkhotel.messages;

/**
 * La fábrica de fuentes de mensaje.
 */
public abstract class MessageSourceFactory {

	private static MessageSourceFactory factory;

	/**
	 * Establece la fábrica de mensajes.
	 */
	public static void setFactory(MessageSourceFactory factoryInstance) {
		factory = factoryInstance;
	}

	/**
	 * Establece la fábrica de mensajes.
	 */
	public static MessageSourceFactory getFactory() {
		return factory;
	}

	/**
	 * Obtiene la fuente de mensajes.
	 */
	public abstract MessageSource getMessageSource();
}
