package cl.checkhotel.web.util;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;

import org.apache.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cl.checkhotel.domain.entity.based.BasedId;
import cl.checkhotel.exceptions.ObjectNotFoundException;
import cl.checkhotel.exceptions.PublicException;

public class CrudControllerTemplate<T extends BasedId<ID>, ID> {

	private static final String ITEM_ID_ATTRIBUTE_NAME = "itemId";

	private static final String REDIRECT = "redirect:/";

	private static final Logger logger = Logger.getLogger(CrudControllerTemplate.class);

	private String domainObjectName;

	private String domainObjectNamePlural;

	private IdGetter<T, ID> idGetter;

	private NewInstaceDomainObjectProvider<T> newInstaceDomainObjectProvider;

	public CrudControllerTemplate(String domainObjectName, String domainObjectNamePlural,
			NewInstaceDomainObjectProvider<T> newInstaceDomainObjectProvider) {
		super();
		this.domainObjectName = domainObjectName;
		this.domainObjectNamePlural = domainObjectNamePlural;
		this.newInstaceDomainObjectProvider = newInstaceDomainObjectProvider;
		this.idGetter = new IdGetter<T, ID>() {

			@Override
			public ID getId(T obj) {
				return obj.getId();
			}
		};
	}

	// @RequestMapping(value = "/", method = RequestMethod.GET)
	public String listAll(Pageable pageable, Model model, Errors errors, ListDataProvider<T> listDataProvider) {

		logger.debug("showAllUsers()");
		if (errors != null && errors.hasErrors())
			return this.domainObjectNamePlural + "/list";

		try {

			Page<T> data = listDataProvider.getData(pageable);

			model.addAttribute(this.domainObjectNamePlural, data);

		} catch (JpaSystemException e) {
			processException(errors, e);
		} catch (ObjectNotFoundException e) {
			processException(errors, e);
		} catch (ConstraintViolationException e) {
			if (errors != null) {
				populateErrors(errors, e.getConstraintViolations());
			} else {
				throw e;
			}
		}

		return this.domainObjectNamePlural + "/list";

	}

	private void processException(Errors errors, RuntimeException e) {
		if (errors != null) {
			populateBindingResultErrors(errors, e.getMessage());
		} else {
			throw e;
		}
	}

	// @RequestMapping(value = "/users", method = RequestMethod.POST)
	public String save(T formBackingObject, BindingResult result, Model model, final RedirectAttributes redirectAttributes,
			Executor<T> executor) {

		logger.debug(MessageFormat.format("save() : [{0}]", formBackingObject));

		saveOrUpdate(getAdd(), formBackingObject, result, model, redirectAttributes, executor);

		return getRedirectoToItemById(formBackingObject);
	}

	public String saveAndList(T formBackingObject, BindingResult result, Model model,
			final RedirectAttributes redirectAttributes, Executor<T> executor) {

		logger.debug(MessageFormat.format("save() : [{0}]", formBackingObject));

		return saveOrUpdate(getAdd(), formBackingObject, result, model, redirectAttributes, executor);
		
	}

	// @RequestMapping(value = "/users", method = RequestMethod.PUT)
	public String update(T formBackingObject, BindingResult result, Model model, final RedirectAttributes redirectAttributes,
			Executor<T> executor) {

		logger.debug(MessageFormat.format("update() : [{0}]", formBackingObject));

		return saveOrUpdate(getUpdate(), formBackingObject, result, model, redirectAttributes, executor);

	}

	private String getRedirectoToItemById(T formBackingObject) {
		return REDIRECT + this.domainObjectNamePlural;
	}

	protected String saveOrUpdate(String currentView, T formBackingObject, BindingResult result, Model model,
			final RedirectAttributes redirectAttributes, Executor<T> executor) {

		logger.debug(MessageFormat.format("saveOrUpdate() : [{0}]", formBackingObject));

		if (result.hasErrors()) {
			populateFormBackingObject(model, formBackingObject);
			return currentView;
		}

		try {
			executor.execute(formBackingObject);
		} catch (ConstraintViolationException e) {
			populateErrors(result, e.getConstraintViolations());
			populateFormBackingObject(model, formBackingObject);

			return currentView;

		} catch (PublicException e) {
			populateBindingResultErrors(result, e.getMessage());
			populateFormBackingObject(model, formBackingObject);

			return currentView;
		}

		model.asMap().clear();

		return getRedirectoToItemById(formBackingObject);
	}

	private ID getId(T formBackingObject) {
		return idGetter.getId(formBackingObject);
	}

	// @RequestMapping(value = "/users", params = "form", method =
	// RequestMethod.GET)
	public String createForm(Model model, T formBackingObject) {

		logger.debug("createForm()");

		populateFormBackingObject(model, formBackingObject);

		return getAdd();

	}

	public String createForm(Model model) {

		return createForm(model, newInstaceDomainObjectProvider.getNewInstance());

	}

	private String getAdd() {
		return this.domainObjectNamePlural + "/add";
	}

	// @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
	public String updateForm(Model model, T formBackingObject) {

		logger.debug("updateForm()");

		populateFormBackingObject(model, formBackingObject);

		return getUpdate();

	}

	private String getUpdate() {
		return this.domainObjectNamePlural + "/update";
	}

	// delete user
	// @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
	public String delete(ID formBackingObjectId, Model model, Executor<ID> executor) {

		// logger.debug("deleteUser() : {}", id);
		logger.debug(MessageFormat.format("delete() : [{0}]", formBackingObjectId));

		executor.execute(formBackingObjectId);

		model.asMap().clear();

		return getRedirectToIndex();

	}

	// show
	// @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
	public String index(Model model) {

		T formBackingObject = newInstaceDomainObjectProvider.getNewInstance();

		logger.debug(MessageFormat.format("index() : [{0}]", formBackingObject));

		model.addAttribute(this.domainObjectName, formBackingObject);

		return getIndex();

	}

	private String getIndex() {
		return this.domainObjectNamePlural + "/index";
	}

	@Deprecated
	public String view(Model model, Errors result, T formBackingObject, FindByIdExecutor<T, ID> findByIdExecutor) {
		return find(model, result, formBackingObject, findByIdExecutor);
	}

	public String find(Model model, Errors result, T formBackingObject, FindByIdExecutor<T, ID> findByIdExecutor) {
		ID id = getId(formBackingObject);

		logger.debug(MessageFormat.format("find() : [{0}]", id));

		if (result.hasErrors()) {
			populateFormBackingObject(model, formBackingObject);
			return getIndex();
		}

		try {
			formBackingObject = findByIdExecutor.execute(id);
		} catch (ConstraintViolationException e) {
			populateErrors(result, e.getConstraintViolations());
			populateFormBackingObject(model, formBackingObject);

			return getIndex();
		} catch (ObjectNotFoundException e) {
			if (null != result) {
				result.rejectValue("id", e.getMessage());
			}
			populateFormBackingObject(model, formBackingObject);
			return getIndex();
		} catch (PublicException e) {
			populateBindingResultErrors(result, e.getMessage());
			populateFormBackingObject(model, formBackingObject);

			return getIndex();
		}

		populateFormBackingObject(model, formBackingObject);
		model.addAttribute(ITEM_ID_ATTRIBUTE_NAME, id);

		return getRedirectView(id);

	}

	public String show(Model model,T formBackingObject) {

		logger.debug(MessageFormat.format("show() : [{0}]", formBackingObject));

		populateFormBackingObject(model, formBackingObject);

		model.addAttribute(ITEM_ID_ATTRIBUTE_NAME, getId(formBackingObject));

		return getView();

	}

	private String getView() {
		return this.domainObjectNamePlural + "/view";
	}

//	public String view(Model model, Errors result, ID id, FindByIdExecutor<T, ID> findByIdExecutor) {
//
//		T formBackingObject = newInstaceDomainObjectProvider.getNewInstance();
//
//		logger.debug(MessageFormat.format("view() : [{0}]", id));
//
//		if (result.hasErrors()) {
//			formBackingObject.setId(id);
//			populateEditForm(model, formBackingObject);
//			return getIndex();
//		}
//		try {
//			formBackingObject = findByIdExecutor.execute(id);
//		} catch (ConstraintViolationException e) {
//			populateErrors(result, e.getConstraintViolations());
//			populateEditForm(model, formBackingObject);
//			return getIndex();
//		} catch (ObjectNotFoundException e) {
//			populateBindingResultErrors(result, e.getMessage());
//			populateEditForm(model, formBackingObject);
//			return getIndex();
//		} catch (PublicException e) {
//			populateBindingResultErrors(result, e.getMessage());
//			populateEditForm(model, formBackingObject);
//
//			return getIndex();
//		}
//
//		populateEditForm(model, formBackingObject);
//		model.addAttribute(ITEM_ID_ATTRIBUTE_NAME, id);
//
//		return getRedirectView(id);
//	}

	private String getRedirectView(ID id) {
		return REDIRECT + this.domainObjectNamePlural + "/" + id.toString();
	}

	private String getRedirectToIndex() {
		return REDIRECT + this.domainObjectNamePlural;
	}

	private static void populateErrors(Errors errors, Set<ConstraintViolation<?>> constraintViolations) {
		for (ConstraintViolation<?> violation : constraintViolations) {
			Path.Node node = null;

			Iterator<Path.Node> iterator = violation.getPropertyPath().iterator();

			try {
				while (iterator.hasNext()) {
					node = iterator.next();

					errors.pushNestedPath(node.getName() == null ? "" : node.getName());

				}
				logger.debug("violation: " + violation.toString());
				String message = violation.getMessage();
				if (logger.isDebugEnabled()) {
					logger.debug(
							"message[ " + node.getName() + "" + violation.getPropertyPath() + " - " + violation.getMessageTemplate() + "]");
				}
				errors.rejectValue(null, message, message);
			} finally {
				errors.setNestedPath("");
			}
		}
	}

	protected void populateBindingResultErrors(Errors errors, String message) {
		errors.reject(message, message);
	}

	protected void populateFormBackingObject(Model model, T formBackingObject) {
		model.addAttribute(this.domainObjectName, formBackingObject);
	}

	public interface Executor<ID> {

		void execute(ID id);

	}

	public interface FindByIdExecutor<T, ID> {

		T execute(ID id);

	}

	public interface ListDataProvider<T> {

		/**
		 * 
		 * @param parms
		 * @return
		 */
		Page<T> getData(Pageable pageable);

	}

	/**
	 * Interface para clases que implementan getters de identificadores.
	 */
	public interface IdGetter<T, ID> {

		/**
		 * Obtiene el identificador del objeto especificado.
		 */
		ID getId(T obj);
	}

	public interface NewInstaceDomainObjectProvider<T> {
		/**
		 * permite generar nuevas instancias al CrudControllerTemplate para
		 * establecer para su funcionamiento
		 * 
		 * @return
		 */
		T getNewInstance();
	}
}
