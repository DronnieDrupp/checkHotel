package cl.checkhotel.web.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * Representa un helper para operaciones web.
 */
public abstract class WebHelper {

	/**
	 * El tamaño máximo de página de datos.
	 */
	public static final int MAX_PAGE_SIZE = 20;

	private static final String ENC = "UTF-8";

	/**
	 * Obtiene una representación como diccionario decodificado del querystring
	 * especificado.
	 * 
	 * <p>
	 * Equivalente a invocar <code>buildQueryStringMap</code> con enc = "UTF-8".
	 * 
	 * @param queryString
	 *            El querystring codificado.
	 */
	public static Map<String, List<String>> buildQueryStringMap(
			String queryString) {
		return buildQueryStringMap(queryString, ENC);
	}

	/**
	 * Obtiene una representación como diccionario decodificado del querystring
	 * especificado.
	 * 
	 * @param queryString
	 *            El querystring codificado.
	 * @param enc
	 *            La codificación usada en el querystring.
	 */
	public static Map<String, List<String>> buildQueryStringMap(
			String queryString, String enc) {
		if (StringUtils.isBlank(queryString))
			return Collections.emptyMap();

		Map<String, List<String>> result = new HashMap<String, List<String>>();

		String[] pairs = queryString.split("&");
		for (String pair : pairs) {
			int index = pair.indexOf("=");

			if (index > 0) {
				add(pair.substring(0, index), pair.substring(index + 1),
						result, enc);
			} else {
				add(pair, "", result, enc);
			}
		}

		return result;
	}

	private static void add(String name, String value,
			Map<String, List<String>> result, String enc) {
		name = decode(name, enc);
		value = decode(value, enc);

		List<String> values = result.get(name);

		if (values == null) {
			values = new ArrayList<String>();

			result.put(name, values);
		}

		values.add(value);
	}

	private static String decode(String data, String enc) {
		try {
			return URLDecoder.decode(data, enc);
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException(e);
		}
	}

}
