package cl.checkhotel.web.backend.config;

public class AppConfigManager {

	private Long codClient;
	
	private String emailClient;

	public Long getCodClient() {
		return codClient;
	}

	public void setCodClient(Long codClient) {
		this.codClient = codClient;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient) {
		this.emailClient = emailClient;
	}
	
	
}
