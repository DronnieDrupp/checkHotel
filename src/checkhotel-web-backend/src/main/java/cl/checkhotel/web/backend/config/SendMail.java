package cl.checkhotel.web.backend.config;

import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

public class SendMail {
	
	private MailSender mailSender;

	public void setMailSender(MailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void sendMail(SimpleMailMessage message) {

		mailSender.send(message);
	}

}
