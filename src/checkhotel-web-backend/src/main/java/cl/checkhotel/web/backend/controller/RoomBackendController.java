package cl.checkhotel.web.backend.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.domain.entity.bussines.TypeEntity;
import cl.checkhotel.services.adm.PalaceServices;
import cl.checkhotel.services.adm.RoomServices;
import cl.checkhotel.services.adm.TypeServices;
import cl.checkhotel.web.util.CrudControllerTemplate;
import cl.checkhotel.web.util.CrudControllerTemplate.Executor;
import cl.checkhotel.web.util.CrudControllerTemplate.ListDataProvider;
import cl.checkhotel.web.util.CrudControllerTemplate.NewInstaceDomainObjectProvider;

@Controller
@RequestMapping("palaces/{palaceId}/rooms")
public class RoomBackendController {
	
	private static final Logger logger = LoggerFactory.getLogger(RoomBackendController.class);
	
	private static final Integer IND_ROOM = 0;

	@Autowired
	private RoomServices roomServices;
	
	@Autowired
	private TypeServices typeServices;
	
	@Autowired
	private PalaceServices palaceServices;
	
	private CrudControllerTemplate<RoomEntity, Long> template;

	public RoomBackendController() {
		template = new CrudControllerTemplate<RoomEntity, Long>("roomEntity", "rooms",
				new NewInstaceDomainObjectProvider<RoomEntity>() {

					@Override
					public RoomEntity getNewInstance() {
						return new RoomEntity();
					}
				});
	}

	@RequestMapping
	public String list(@PathVariable("palaceId") final Long palaceId,
			@PageableDefault(direction = Direction.DESC, sort = { "description" }) Pageable pageable, Model model) {
		
		model.addAttribute("palaceId", palaceId);
		model.addAttribute("palace", getPalaceById(palaceId));

		return template.listAll(pageable, model, (Errors) null,
				new ListDataProvider<RoomEntity>() {

					@Override
					public Page<RoomEntity> getData(Pageable pageable) {
						return roomServices.listRoomEntities(palaceId, pageable);
					}

				});
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String create(Model model, @PathVariable("palaceId") final Long palaceId) {
		
		loadData(model, null);
		
		RoomEntity room = new RoomEntity();
		PalaceEntity palace = new PalaceEntity();
		
		palace.setId(palaceId);
		
		room.setPalace(palace);
		
		return template.createForm(model, room);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(RoomEntity roomEntity, BindingResult result, @PathVariable("palaceId") final Long palaceId,  Model model) {
		
		PalaceEntity palace = new PalaceEntity();
		palace.setId(palaceId);
		
		roomEntity.setPalace(palace);
		
		String returnTemplateSave = template.saveAndList(roomEntity, result, model, null, new Executor<RoomEntity>(){

			@Override
			public void execute(RoomEntity roomEntity) {
				roomServices.saveRoom(roomEntity);
				
			}
			
		});
		
		loadData(model, result);
		
		if (StringUtils.startsWithIgnoreCase(returnTemplateSave, "redirect:/")) {
			returnTemplateSave = StringUtils.replace(returnTemplateSave, "redirect:", "redirect:/palaces/" + palaceId);
		}
		
		return returnTemplateSave;
		
	}

	@RequestMapping(value = "/{roomId}/update", method = RequestMethod.GET)
	public String updateForm(@PathVariable("roomId") final Long roomId, Model model) {
		
		loadData(model, null);
		
		RoomEntity roomEntity = roomServices.roomById(roomId);
		
		return template.updateForm(model, roomEntity);		
	}	
	
	@RequestMapping(value = "/{roomId}/save", method = RequestMethod.POST)
	public String update(@PathVariable("roomId") final Long roomId,RoomEntity roomEntity,@PathVariable("palaceId") final Long palaceId,BindingResult result,  Model model) {
		
		

		String returnTemplateSave = template.saveAndList(roomEntity, result, model, null, new Executor<RoomEntity>(){

			@Override
			public void execute(RoomEntity roomEntity) {
				
				roomEntity.setId(roomId);
				
				roomServices.updateRoom(roomEntity);
			}
			
		});
		
		loadData(model, result);
		
		if (StringUtils.startsWithIgnoreCase(returnTemplateSave, "redirect:/")) {
			returnTemplateSave = StringUtils.replace(returnTemplateSave, "redirect:", "redirect:/palaces/" + palaceId);
		}
		
		return returnTemplateSave;
		
	}

	@RequestMapping(value = "/{roomId}/delete", method = RequestMethod.DELETE)
	public String delete(@PathVariable("roomId") final Long roomId, @PathVariable("palaceId") final Long palaceId,  Model model) {

		return returnPath(template.delete(roomId, model, new Executor<Long>(){

			@Override
			public void execute(Long roomId) {
				roomServices.deleteRoom(roomId);
				
			}
			
		})
		, palaceId);
		
	}
	
	public String returnPath(String redirect, Long idPalace){
		
		if (StringUtils.startsWithIgnoreCase(redirect, "redirect:/")) {
			redirect = StringUtils.replace(redirect, "redirect:", "redirect:/palaces/" + idPalace);
		}
		
		return redirect;
	}

	private List<TypeEntity> getListType() {
		
		return (List<TypeEntity>)typeServices.findByIndicator(IND_ROOM);
		
	}
	
	private void loadData(Model model,Errors result) {
		if (null == result || result.hasErrors()){
			model.addAttribute("listType", getListType());
		}
	}
	
	private PalaceEntity getPalaceById(Long palaceId) {
		return palaceServices.palaceById(palaceId);
	}

}
