package cl.checkhotel.web.backend.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.services.adm.DistrictServices;
import cl.checkhotel.services.adm.PalaceServices;
import cl.checkhotel.services.adm.TypeServices;
import cl.checkhotel.web.backend.config.AppConfigManager;
import cl.checkhotel.web.util.CrudControllerTemplate;
import cl.checkhotel.web.util.CrudControllerTemplate.Executor;
import cl.checkhotel.web.util.CrudControllerTemplate.ListDataProvider;
import cl.checkhotel.web.util.CrudControllerTemplate.NewInstaceDomainObjectProvider;

@Controller
@RequestMapping("/palaces")
public class PalacesBackendController {

	private static final int PALACE_TYPE = 1;

	@Autowired
	private PalaceServices palaceServices;
	
	@Autowired
	private DistrictServices districtServices;

	@Autowired
	private AppConfigManager appConfigManager;
	
	@Autowired
	private TypeServices typeServices;

	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(PalacesBackendController.class);

	private CrudControllerTemplate<PalaceEntity, Long> template;

	public PalacesBackendController() {
		template = new CrudControllerTemplate<PalaceEntity, Long>("palaceEntity",
				"palaces", new NewInstaceDomainObjectProvider<PalaceEntity>() {

					@Override
					public PalaceEntity getNewInstance() {
						return new PalaceEntity();
					}
				});
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String list(@PageableDefault(direction = Direction.DESC, sort = { "description" }) Pageable pageable, Model model) {
		
		return template.listAll(pageable, model, (Errors) null,
				new ListDataProvider<PalaceEntity>() {

					@Override
					public Page<PalaceEntity> getData(Pageable pageable) {
						return palaceServices.listPalaceEntities(appConfigManager.getCodClient(), pageable);
					}

				});
	}

	@RequestMapping(params="form", method = RequestMethod.GET)
	public String createForm(Model model) {
		
		populateData(model,null);
		return template.createForm(model, new PalaceEntity());
	}

	@RequestMapping(method = RequestMethod.POST)
	public String saveAndList(PalaceEntity palaceEntity,BindingResult result,  Model model) {

		String returnValue = template.saveAndList(palaceEntity, result, model, null, new Executor<PalaceEntity>(){

			@Override
			public void execute(PalaceEntity palaceEntity) {
				palaceEntity.setClient(appConfigManager.getCodClient());
				palaceServices.savePalace(palaceEntity);
				
			}
			
		});
		populateData(model,result);
		return returnValue;
		
	}	
	
	@RequestMapping(value = "/{id}", params="form", method = RequestMethod.GET)
	public String updateForm(@PathVariable("id") final Long palaceId, Model model) {
		
		populateData(model,null);
		PalaceEntity palaceEntity=palaceServices.palaceById(palaceId);
		
		return template.updateForm(model, palaceEntity);		
	}	

	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public String update(@PathVariable("id") final Long palaceId,PalaceEntity palaceEntity, BindingResult result,  Model model) {

		String saveAndList = template.update(palaceEntity, result, model, null, new Executor<PalaceEntity>(){

			@Override
			public void execute(PalaceEntity palaceEntity) {
				palaceEntity.setId(palaceId);
				palaceServices.updatePalace(palaceEntity);
				
			}
			
		});
		populateData(model,result);
		return saveAndList;
		
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable("id") final Long palaceId,  Model model) {

		return template.delete(palaceId, model, new Executor<Long>(){

			@Override
			public void execute(Long palaceId) {
				palaceServices.deletePalace(palaceId);
				
			}
			
		});
		
	}

	private void populateData(Model model,Errors result) {
		if (null == result || result.hasErrors()){
			model.addAttribute("comunas", districtServices.listAll());
			model.addAttribute("tipoHospedaje",typeServices.findByIndicator(PALACE_TYPE));
		}
	}	
}
