package cl.checkhotel.web.backend.controller;

import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import cl.checkhotel.domain.entity.bussines.BookingEntity;

@Controller
@RequestMapping("/editarBooking")
public class BookingEditController {
	
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(BookingBackendController.class);

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {

		String now = (new Date()).toString();
		logger.info("BookingController " + now);

		model.addAttribute("bookingEntity", new BookingEntity());

		return new ModelAndView("booking/editar");
	}
}
