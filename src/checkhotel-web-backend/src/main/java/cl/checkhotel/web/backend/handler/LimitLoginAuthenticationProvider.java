package cl.checkhotel.web.backend.handler;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import cl.checkhotel.dao.jpa.adm.UserDetailsDao;
import cl.checkhotel.domain.user.UserAttempts;

public class LimitLoginAuthenticationProvider extends DaoAuthenticationProvider {

	UserDetailsDao userDetailsDao;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {

			Authentication auth = super.authenticate(authentication);
			// if reach here, means login success, else exception will be thrown
			// reset the user_attempts
			userDetailsDao.resetFailAttempts(authentication.getName());
//			limitAuthenticationService.resetAttempts(getNormalizedUserName(authentication));
			return auth;

		} catch (BadCredentialsException e) {

			 userDetailsDao.updateFailAttempts(authentication.getName());
//			limitAuthenticationService.updateAttempts(getNormalizedUserName(authentication));
			throw e;

		} catch (LockedException e) {

			String error = "";
			UserAttempts userAttempts = userDetailsDao.getUserAttempts(getNormalizedUserName(authentication));
//			UserAttempts userAttempts = limitAuthenticationService.getUserAttempts(getNormalizedUserName(authentication));
			if (userAttempts != null ) {
				Date lastAttempts = userAttempts.getLastModified();
				error = "User account is locked! <br><br>Username : " + getNormalizedUserName(authentication)
						+ "<br>Last Attempts : " + lastAttempts;
			} else {
				error = e.getMessage();
			}

//			throw new LockedException(error);
			throw e;
		}

	}

	private String getNormalizedUserName(Authentication authentication) {
		return StringUtils.remove(StringUtils.remove(authentication.getName(), "."), "-");
	}

	public void setUserDetailsDao(UserDetailsDao userDetailsDao) {
		this.userDetailsDao = userDetailsDao;
	}

}