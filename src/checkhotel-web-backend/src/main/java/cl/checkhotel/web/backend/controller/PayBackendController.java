package cl.checkhotel.web.backend.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.checkhotel.domain.basicEnum.PaymentType;
import cl.checkhotel.domain.basicEnum.Status;
import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.PayEntity;
import cl.checkhotel.services.book.BookingServices;
import cl.checkhotel.services.pay.PayService;
import cl.checkhotel.web.util.CrudControllerTemplate;
import cl.checkhotel.web.util.CrudControllerTemplate.Executor;
import cl.checkhotel.web.util.CrudControllerTemplate.NewInstaceDomainObjectProvider;

@Controller
@RequestMapping("bookings/{bookingsId}/pays")
public class PayBackendController {

	@Autowired
	private BookingServices bookingServices;

	@Autowired
	private PayService payService;

	private CrudControllerTemplate<PayEntity, Long> template;

	public PayBackendController() {
		template = new CrudControllerTemplate<PayEntity, Long>("payEntity",
				"pays", new NewInstaceDomainObjectProvider<PayEntity>() {

					@Override
					public PayEntity getNewInstance() {
						return new PayEntity();
					}
				});
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				new SimpleDateFormat("dd-MM-yyyy"), true, 10));
	}

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model,
			@PathVariable("bookingsId") final Long bookingsId) {
		loadData(model, bookingsId, null);

		return template.index(model);
	}

	private PayEntity getPayEntity(Long bookingsId) {
		PayEntity pay = payService.searchPayByBooking(bookingsId);

		if (null == pay) {
			pay = new PayEntity();
		}
		return pay;
	}

	private BigDecimal getTotalPay(Long bookingsId) {

		BookingEntity booking = getBookingId(bookingsId);

		return booking.getTotalBook();
	}

	private BookingEntity getBookingId(Long bookingsId) {
		// TODO Auto-generated method stub
		return bookingServices.searchIdBooking(bookingsId);
	}

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String createBook(@PathVariable("bookingsId") final Long bookingsId,
			Model model) {

		loadData(model, bookingsId, null);

		return template.createForm(model, getPayEntity(bookingsId));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(PayEntity payEntity, BindingResult result,
			@PathVariable("bookingsId") final Long bookingsId, Model model) {

		payEntity.setVoucher(StringUtils.isEmpty(payEntity.getVoucher()) ? null : payEntity.getVoucher());
		
		String returnTemplateSave = template.saveAndList(payEntity, result,
				model, null, new Executor<PayEntity>() {

					@Override
					public void execute(PayEntity payEntity) {

						payService.saveOrUpdate(payEntity);

					}

				});

		if (StringUtils.startsWithIgnoreCase(returnTemplateSave, "redirect:/")) {

			BookingEntity book = bookingServices.searchIdBooking(bookingsId);

			book.setStatus(Status.PAID.getDesc());

			bookingServices.updateBooking(book);

			return "redirect:/bookings/" + bookingsId + "/update";
		}

		loadData(model, bookingsId, result);

		return returnTemplateSave;

	}

	@RequestMapping(value = "/{bookingsId}/end", method = RequestMethod.GET)
	public String voucher(@PathVariable("bookingsId") Long bookingsId,
			Model model) {

		return "bookings/update";
	}

	private void loadData(Model model, final Long bookingsId, Errors result) {

		if (null == result || result.hasErrors()) {
			model.addAttribute("bookingsId", bookingsId);
			model.addAttribute("totalPay", getTotalPay(bookingsId));
			model.addAttribute("listTypePay", PaymentType.values());
		}

	}

}
