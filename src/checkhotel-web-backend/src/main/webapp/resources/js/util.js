var legendTemplate = "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%> (<%=segments[i].value%>)<%}%></li><%}%></ul>";

//$(document).ready(function(){
//	
////	bootbox.setLocale("es");
////	bootbox.setBtnClasses({
////	    OK : 'btn-inverse',
////	    CANCEL : 'btn-inverse',
////	    CONFIRM : 'btn-inverse'
////	});
//	
//});


function validaRut(campo){
	if ( campo.length == 0 ){ return false; }
	if ( campo.length < 8 ){ return false; }

	campo = campo.replace('-','')
	campo = campo.replace(/\./g,'')

	var suma = 0;
	var caracteres = "1234567890kK";
	var contador = 0;    
	for (var i=0; i < campo.length; i++){
		u = campo.substring(i, i + 1);
		if (caracteres.indexOf(u) != -1)
		contador ++;
	}
	if ( contador==0 ) { return false }
	
	var rut = campo.substring(0,campo.length-1)
	var drut = campo.substring( campo.length-1 )
	var dvr = '0';
	var mul = 2;
	
	for (i= rut.length -1 ; i >= 0; i--) {
		suma = suma + rut.charAt(i) * mul
                if (mul == 7) 	mul = 2
		        else	mul++
	}
	res = suma % 11
	if (res==1)		dvr = 'k'
                else if (res==0) dvr = '0'
	else {
		dvi = 11-res
		dvr = dvi + ""
	}
	if ( dvr != drut.toLowerCase() ) { return false; }
	else { return true; }
}

function cleanRut(strRut) {
	var rutsgnp = "";

	while ((new Number(strRut.charAt(0)) == 0) && (strRut != "")) {
		strRut = strRut.substring(1, strRut.length);
	}
	var i;
	for (i = 0; i < strRut.length; i++) {
		if (($.isNumeric(strRut.charAt(i)) || (strRut.charAt(i) != "."
				&& strRut.charAt(i) != "-" && strRut.charAt(i) != " " && (!$
				.isNumeric(strRut.charAt(i)) && strRut.charAt(i).toUpperCase() == "K")))) {
			rutsgnp = rutsgnp + strRut.charAt(i);
		}
	}

	return rutsgnp;

}

function rutFormatter(strRut) {
	if (strRut == undefined || strRut.length == 0)
		return;

	strRut = cleanRut(strRut);

	var straux = strRut.substring(strRut.length - 1, strRut.length);
	var rutsgnp = strRut.substring(0, strRut.length - 1);

	var strAuxArray = new Array(0, 0, 0);
	strAuxArray[0] = rutsgnp.substring(rutsgnp.length - 3, rutsgnp.length);
	strAuxArray[1] = rutsgnp.substring(rutsgnp.length - 6, rutsgnp.length - 3);
	strAuxArray[2] = rutsgnp.substring(0, rutsgnp.length - 6);
	var i;
	rutsgnp = "-" + straux;
	for (i = 0; i < 3; i++) {
		if (strAuxArray[i] == "") {
			i = 3;
		} else {
			if (i > 0) {
				rutsgnp = "." + rutsgnp;
			}
			rutsgnp = strAuxArray[i] + rutsgnp;
		}
	}

	return rutsgnp.toUpperCase();
}