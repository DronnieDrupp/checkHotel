function cleanRut(strRut) {
	var rutsgnp = "";

	while ((new Number(strRut.charAt(0)) == 0) && (strRut != "")) {
		strRut = strRut.substring(1, strRut.length);
	}
	var i;
	for (i = 0; i < strRut.length; i++) {
		if (($.isNumeric(strRut.charAt(i)) || (strRut.charAt(i) != "."
				&& strRut.charAt(i) != "-" && strRut.charAt(i) != " " && (!$
				.isNumeric(strRut.charAt(i)) && strRut.charAt(i).toUpperCase() == "K")))) {
			rutsgnp = rutsgnp + strRut.charAt(i);
		}
	}

	return rutsgnp;

}

function rutFormatter(strRut) {
	if (strRut.length == 0)
		return;

	strRut = cleanRut(strRut);

	var straux = strRut.substring(strRut.length - 1, strRut.length);
	var rutsgnp = strRut.substring(0, strRut.length - 1);

	var strAuxArray = new Array(0, 0, 0);
	strAuxArray[0] = rutsgnp.substring(rutsgnp.length - 3, rutsgnp.length);
	strAuxArray[1] = rutsgnp.substring(rutsgnp.length - 6, rutsgnp.length - 3);
	strAuxArray[2] = rutsgnp.substring(0, rutsgnp.length - 6);
	var i;
	rutsgnp = "-" + straux;
	for (i = 0; i < 3; i++) {
		if (strAuxArray[i] == "") {
			i = 3;
		} else {
			if (i > 0) {
				rutsgnp = "." + rutsgnp;
			}
			rutsgnp = strAuxArray[i] + rutsgnp;
		}
	}

	return rutsgnp.toUpperCase();
}

$(document).ready(function() {
	$("form").delegate(".rut", "focus", function() {
		var strRut = $(this).val();
		$(this).val(cleanRut(strRut));
	});

	$("form").delegate(".rut", "blur", function() {
		var strRut = $(this).val();
		if (strRut.length == 0)
			return;

		$(this).val(rutFormatter(strRut));
	});

	$("input.rut").each(function() {
		var strRut = $(this).val();
		if (strRut.length == 0)
			return;

		$(this).val(rutFormatter(strRut));
	});
});