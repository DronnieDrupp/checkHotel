<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<h1><a href="${pageContext.request.contextPath}/">Corretajes Co�aripe</a></h1>
<div class="menu_bar">
<a class="bt-menu" href="#"><span class="icon-list2"></span>Menu</a>
</div>

<nav>
<ul id="menu">
<li><a href="${pageContext.request.contextPath}/">Portada</a></li>
<li><a href="${pageContext.request.contextPath}/bookings">Reservas</a></li>
<li><a href="${pageContext.request.contextPath}/palaces">Centros</a></li>
</ul>
</nav>

<script src="<c:url value='/resources/js/menu.js' />"></script>
<a href="logout">Cerrar sesi�n</a>
<h2>Bienvenido a la plataforma online de Administraci&oacute;n de Reservas de Corretajes Co&ntilde;aripe.&nbsp;</h2>