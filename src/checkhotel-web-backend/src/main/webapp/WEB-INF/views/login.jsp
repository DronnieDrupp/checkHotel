<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="reserva_cont" style="min-height: 300px!important;">
    <h3>Bienvenido</h3>
        <form method="post" action="login" style="margin-top: 30px; display: inline-block;" id="loginForm">
        <ul class="reserva">
			<li>
				<label>RUT:</label>
				<input style="background:#fff!important;" class="rut" name="username" id="username" type="text" placeholder="<spring:message code='sample.email' /> ">
			</li>
			<li>
			<label>CONTRASEÑA:</label>
			<input name="password" id="password" type="password"  placeholder="Password">
			</li>
			<c:if test="${not empty param.error}">
				<li>
					<div class="error-container">
					        ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
					</div>
				</li>
			</c:if>
			<li>
				<button type="submit" name="submit" class="btn btn-inverse btn-block"><spring:message code="ingresar" /></button>
			</li>
		</ul>	
        </form>
   
</div>
<section id="mainRight">
	<div class="foto_intro">
		<img src="${pageContext.request.contextPath}/resources/img/foto01.jpg" alt="" title="" border="0" />
		
	</div>   
</section>
  <script type="text/javascript">
	$(document).ready(function() {
			   
		$("#loginForm").on("submit",function(e){
			
			$("#username").val(cleanRut($("#username").val()));
			
		});
			

		});
	</script>