<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont">
	<h3>SU RESERVA</h3>
</div>
<section id="mainRight">
	<div class="mainEspacio"></div>
	<blockquote>una copia ha sido enviada a su correo ingresado.  La acci�n de eliminaci�n no puede deshacerse.</blockquote>
	
	<form:form action="" method="GET" modelAttribute="bookingEntity">

	<ul class="disponibilidad_lista">
			<li class="one_col">
			<section></section>
			<section>
			<h3 class="estadia">
			<strong>Su reserva ha sido ingresada  satisfactoriamente.</strong><br>
			<p class="${cssMsg}">${message}</p><br><br>
			ID DE RESERVA: <strong>${bookingEntity.id} </strong><br>
			${bookingEntity.roomEntity.description}: <strong>(${bookingEntity.roomEntity.numberPerson} personas)</strong><br>
			Valor por d�a: <strong>$<fmt:formatNumber value="${bookingEntity.roomEntity.price}" type="number"/></strong> - 
			Total estadia completa: <strong>$<fmt:formatNumber value="${bookingEntity.totalBook}" type="number"/></strong>
			</h3>
			
			<h2 style="margin-top: 20px!important;" >DETALLES DEL TITULAR</h2>
			
				<ul>
				<li class="ancho32"><label>Nombre:</label> <h5>${bookingEntity.guestEntity.fullName}</h5></li>
				<li class="ancho32"><label>Rut:</label> <h5>${bookingEntity.guestEntity.legalId}</h5></li>
				<li class="ancho32"><label>Email:</label> <h5>${bookingEntity.guestEntity.email}</h5></li>
				</ul>
			
			<h2 style="margin-top: 20px!important;" >DETALLES DE LA RESERVA</h2>
				<ul>
				<li class="ancho32"><label>Check in:</label> <h5><fmt:formatDate var='formattedDateFrom' value='${bookingEntity.dateFrom}' pattern="dd-MM-yyyy" /> ${formattedDateFrom}</h5></li>
				<li class="ancho32"><label>Check out:</label> <h5><fmt:formatDate	var='formattedDateTo' value='${bookingEntity.dateTo}' pattern="dd-MM-yyyy" /> ${formattedDateTo}</h5></li>
				<li>
		<label>Caracter�sticas:</label> 
		<ul>
		<c:forEach var="strProp" items="${bookingEntity.roomEntity.listProperties}"><li>&raquo; ${strProp}</li></c:forEach>
		</ul></li>
				</ul>
			</section>
			
			<div class="disponibilidad_botonera"><spring:url value="/" var="back_url"/><a class="button" href="${back_url}">Volver</a ></div>
			</li>
		</ul>       
	</form:form>
</section>