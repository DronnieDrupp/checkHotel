<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont" style="height:auto;">
	<h3>Ingresar Pago</h3>
</div>

<section id="mainRight">
<div class="main_espacio"><blockquote></blockquote>
</div> 
		<ul class="disponibilidad_lista">
			<li class="one_col">
				<section></section>
				
				<section>
					<form:form action="save" method="POST" modelAttribute="payEntity">
					<h3 class="estadia">
						ID DE RESERVA: <strong>${bookingsId}</strong>
						TOTAL PAGO ESTADIA: <strong>$<fmt:formatNumber value="${totalPay}" type="number"/></strong>
						<form:input type="hidden" path="booking" size="12" value="${bookingsId}" />
						<form:input type="hidden" path="paymentAmount" size="12" value="${totalPay}" />
					</h3>
					
					
						<ul class="reserva">
							<li>
								<label>N�mero de Comprobante o Boleta</label>
								<form:input type="text" path="voucher" size="12" value="${payEntity.voucher}" />
								<chforms:fieldError modelAttribute="payEntity" path="voucher"/>
							</li>
							<li>
								<label>N�mero Tarjeta</label>
								<form:input type="text" path="numerdCard" size="12" value="${payEntity.numerdCard}" />
								<chforms:fieldError modelAttribute="payEntity" path="numerdCard"/>
							</li>
							<li class="ancho32">
								<label style="font-size:1em;">Tipo de Pago</label>
									
								<form:select path="paymentType">
								    <form:option value="0" label="Selecciona" />
								    <form:options items="${listTypePay}" itemValue="id" itemLabel="desc"/>
								</form:select>
<%-- 								<chforms:fieldError modelAttribute="payEntity" path="paymentType"/> --%>
								 
							</li>
							<div class="disponibilidad_botonera">
								<spring:url value="/bookings/${bookingsId}/update" var="back_url"/>
								
									<a class="button" href="${back_url}">Volver</a >
									
									<input type="submit" style="width:auto!important;" class="button" title="Pagar" value="Adjuntar" />
							</div>

						</ul>
					</form:form>
				</section>
			</li>
		</ul>
	

</section>

