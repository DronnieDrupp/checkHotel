<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont">
	<h3>Reservas</h3>
</div>

<section id="mainRight">
	<div class="mainEspacio" ></div> 
	<blockquote></blockquote>
	
	

	<ul class="disponibilidad_lista">
		<li class="one_col">
		<section></section>
		<section>
		
		<c:forEach items="${bookings.content}" var="booking" varStatus="loopStatus">
		<ul>
		<li class="ancho32"><label>Nombre Pasajero:</label> <h5>${booking.guestEntity.fullName}</h5></li>
		<li class="ancho32"><label>Identificaci�n Legal:</label> <h5>${booking.guestEntity.legalId}</h5></li>
		<li class="ancho32"><label>Caba�a:</label> <h5>${booking.roomEntity.description}</h5></li>
		<li class="ancho32"><label>Fecha Inicio:</label> <h5>${booking.dateFrom}</h5></li>
		<li class="ancho32"><label>Fecha Termino:</label> <h5>${booking.dateTo}</h5></li>
		<li class="ancho32"><label>Estado:</label> <h5>${booking.status}</h5></li>
		
		<div class="disponibilidad_botonera">
			<spring:url value="bookings/${booking.id}/update" var="update_form_url"/>
			<spring:url value="bookings/${booking.id}/delete" var="delete_form_url" />
			<form:form action="${delete_form_url}" method="DELETE" >
			<input class="button" type="submit" value="Eliminar" style="width: auto;"  class="delete-button"/>
			</form:form><a href="${update_form_url}" class="button">Modificar</a>
		</div>
		
		
		</ul>
	</c:forEach>
		
		
		</section>
		</li>
	</ul>
	

	<ul class="disponibilidad_lista">
		<li>
			<h3>Nueva Reserva</h3>
				<spring:url value="/bookings/Rooms" var="updateUrl2" />
				<form:form action="${updateUrl2}" method="POST" modelAttribute="objectSearch">
					<ul class="reserva">
						<li>
							<label>Check in:</label>
							<fmt:formatDate	var='formattedDateFrom' value='${objectSearch.dateFrom}' pattern="dd-MM-yyyy" /> 
							<form:input path="dateFrom" type="text"	size="10" name="datepicker" id="datepicker"	value="${formattedDateFrom}" readonly="true" /> 
							<chforms:fieldError modelAttribute="objectSearch" path="dateFrom" />
						</li>
						<li>
							<label>Check out:</label>
							<fmt:formatDate	var='formattedDateTo' value='${objectSearch.dateTo}' pattern="dd-MM-yyyy" /> 
							<form:input path="dateTo" type="text" size="10" name="datepicker" id="datepicker2" value="${formattedDateTo}"  readonly="true" /> 
							<chforms:fieldError	modelAttribute="objectSearch" path="dateTo" />
						</li>
						<li>
							<label>Hu�spedes:</label>  
							<form:input path="numberPerson" type="number" size="10"/>
							<chforms:fieldError	modelAttribute="objectSearch" path="numberPerson" /> 
						</li>
						<li>
							<button type="submit" >Ver Disponibilidad</button>
						</li>	
					</ul>
				</form:form>
		</li>
	</ul>
</section>
<script type="text/javascript">
$(document).ready(function() {
   $("#datepicker").datepicker({ minDate: 0 });
   $("#datepicker2").datepicker({ minDate: 0 });
 });
</script>
