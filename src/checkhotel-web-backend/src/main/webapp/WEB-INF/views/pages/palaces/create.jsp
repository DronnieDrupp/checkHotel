<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont">
<h3>INGRESO NUEVO HOTEL</h3>
</div>

<section id="mainRight">
<div class="main_espacio">
<blockquote>
		Recuerda, antes de ingresar un nuevo hotel debes asegurarte que ya
		existe el proveedor. Si no existe, ingr�salo antes de registrar una
		nueva propiedad <a href="admin_nuevo_proveedor.html">&raquo; aqu��</a>
</blockquote>
</div>

<ul class="disponibilidad_lista">
	<li class="one_col">
		<h3>REGISTRO</h3>		
		<section></section>
		<section>
			<form:form action="palaces" method="POST" modelAttribute="palaceEntity">
				<ul class="reserva">
					<li class="ancho48">
						<label>Nombre del Hotel</label>
						<form:input type="text" path="description" size="100" />
						<chforms:fieldError modelAttribute="palaceEntity" path="description"/>		
					</li>
					<li class="ancho48">
						<label>Responsable</label>
						<form:input type="text" path="manager" size="100" />
						<chforms:fieldError modelAttribute="palaceEntity" path="manager"/>
					</li>
					<li>
						<label>Direcci&oacute;n</label>
						<form:input type="text" path="address" id="address" size="100" />
						<chforms:fieldError modelAttribute="palaceEntity" path="address"/> 
					</li>
					
					<li class="ancho32"><label>Comuna</label>
						<form:select id="selectmenu3" path="district.id">
						<form:option value="0" label="Selecciona" />
						<form:options items="${comunas}" itemLabel="description" itemValue="id"></form:options>
						</form:select>
					</li>
					<li class="ancho32" >
						<label>Tel&eacute;fono</label>
						<form:input type="text" path="phone" id="phone" size="12" />
						<chforms:fieldError modelAttribute="palaceEntity" path="phone"/>	
					</li>
					<li class="ancho32" >
						<label>Estrellas</label>
						<form:select id="selectmenu3" path="starsNumber">
							<form:option value="0" label="Selecciona" />
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
						</form:select>
					</li>
					<li class="ancho32" >
						<label style="font-size:1em;">Tipo de Hospedaje</label>
						<form:select id="selectmenu3" path="type">
						<form:options items="${tipoHospedaje}" itemLabel="description" itemValue="id"></form:options>
						</form:select>									
					</li>
					<li style="width:99%!important;" >
						<label>Caracteristicas <span>(separar cada una con comas ",")</span></label>
						<form:textarea rows="5" cols="100" path="properties"></form:textarea>
					</li>
						
					<li style="width:99%!important;" >
						<label>Comentarios</label>
						<form:textarea rows="5" cols="100" path="comment" ></form:textarea>
						<chforms:fieldError modelAttribute="palaceEntity" path="comment"/>
					</li>
						<div class="disponibilidad_botonera">
							<input type="submit" class="button" title="Ingresar Nuevo Hotel" style="width:auto!important;" />
						</div>
				</ul>
				
			</form:form>
		</section>
	</li>
</ul>	
</section>
