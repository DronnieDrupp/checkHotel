<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont">
	<h3>MODIFICAR RESERVA</h3>
</div>
<section id="mainRight">
	<div class="mainEspacio">
	<blockquote>Aquí puedes modificar tu reserva.</blockquote>
	</div>
	
	<spring:url value="/bookings/${id}" var="updateUrl2" />
	<form:form action="${updateUrl2}" method="PUT" modelAttribute="bookingEntity">
	
	<ul class="disponibilidad_lista">
	<li class="one_col">
	<section></section>
	<section>
		<h3 class="estadia">
		${bookingEntity.roomEntity.description} <strong>${bookingEntity.roomEntity.price}</strong></br>
		ID DE RESERVA: <strong>${id}</strong></br>
		ESTADO DE LA RESERVA: <strong>${bookingEntity.status}</strong>
		</h3>
				
		<h2 style="margin: 20px 0px 0px 0px!important;" >DETALLES DEL TITULAR</h2>
				<ul>
				<li class="ancho48"><label>Nombre(s):</label>
				<form:input path="guestEntity.firstName" type="text" size="40" />
				</li>
				<li class="ancho48"><label>Apellido(s):</label>
				<form:input path="guestEntity.lastName" type="text" size="40" />
				</li>
				<li class="ancho48"><label>Rut o Identificación <span style="line-height: 14px;">(sin puntos ni guiones):</span></label>
				<form:input path="guestEntity.legalId" type="text" size="12" />
				<chforms:fieldError modelAttribute="booking" path="guestEntity.legalId" />
				</li>
				<li class="ancho48"><label>Email:</label>
				<form:input path="guestEntity.email" type="text" size="40" />
				<chforms:fieldError modelAttribute="booking" path="guestEntity.email" />
				<form:input path="guestEntity.active" type="hidden" value="true"/>
				</li>
				</ul>

		<h2 style="margin: 20px 0px 0px 0px!important;" >DETALLES DE LA RESERVA</h2>
				<ul>
				<li class="ancho48">	
				<label>Check in:</label> 
				<fmt:formatDate	var='formattedDateFrom' value='${bookingEntity.dateFrom}' pattern="dd-MM-yyyy" /> 
				<form:input path="dateFrom" type="text"	size="10" name="datepicker" id="datepicker"	value="${formattedDateFrom}" /> 
				<chforms:fieldError modelAttribute="bookingEntity" path="dateFrom" />
				</li>
				
				<li class="ancho48">
				<label>Check out:</label> 
				<fmt:formatDate	var='formattedDateTo' value='${bookingEntity.dateTo}' pattern="dd-MM-yyyy" /> 
				<form:input path="dateTo" type="text"	size="10" name="datepicker" id="datepicker2" value="${formattedDateTo}" /> 
				<chforms:fieldError	modelAttribute="bookingEntity" path="dateTo" />
				</li>

				
				<li>
				<label>Características:</label>
					<ul>
					<c:forEach var="strProp" items="${bookingEntity.roomEntity.listProperties}">
					<li>&raquo; ${strProp}</li>
					</c:forEach>
					</ul>
				</li>
				</ul>

	<div class="disponibilidad_botonera">					
			<form:input path="roomEntity.id" type="hidden" value="${bookingEntity.roomEntity.id}"/>
			<spring:url value="/bookings/${itemId}" var="back_url"/>
			<a class="button" href="${back_url}">Volver</a >
			
			<button type="submit"  class="button" >Modificar Reserva</button>
			
			<spring:url value="/bookings/${bookingEntity.id}/pays/add" var="pay_url"/>
			<a class="button" href="${pay_url}">Adjuntar Pago</a>
	</div>


	</section>			
	</li>
	</ul>       
	</form:form>
</section>
<script type="text/javascript">
	$(document).ready(function() {
	   $("#datepicker").datepicker();
	   $("#datepicker2").datepicker();
	 });
 </script>