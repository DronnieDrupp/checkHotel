<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<div class="reserva_cont">
<h3>Editar Caba�a</h3>
</div>

<section id="mainRight">
<div class="main_espacio"><blockquote></blockquote></div> 
	
<form:form action="save" method="POST" modelAttribute="roomEntity">
<ul class="disponibilidad_lista">
<li class="one_col">
<h3>INFORMACI�N REGISTRADA</h3>
<section></section>

<section>


				<ul class="reserva">
				<li>
				<label>Nombre</label>
				<form:input type="text" path="description" size="100" />
				</li>
				
				<li>
				<label>Direcci�n</label>
				<form:input type="text" path="address" size="100" />
				</li>
				
				<li class="ancho32">
				<label style="font-size:1em;">Tipo de Habitaci�n</label>
				<form:select path="type">
				<form:option value="0" label="Selecciona" />
				<form:options items="${listType}" itemValue="id" itemLabel="description" />
				</form:select>
				</li>
				
				<li class="ancho32">
				<label>Valor Diario</label>
				<form:input type="number" path="price" size="12" />
				</li>
				
				<li class="ancho32">
				<label>Capacidad</label>
				<form:input type="number" path="numberPerson" size="12" />
				</li>
				
				<li>
				<label>Caracteristicas <span>(Escriba las caracteristicas separadas por una ",")</span></label>
				<form:textarea rows="5" cols="100" path="properties"></form:textarea>
				</li>
				
				<li>
				<label>Descripci�n</label>
				<form:textarea rows="5" cols="100" path="comment"></form:textarea>
				</li>
				
				<li>
				<label>Link Galer�a</label>
				<form:input type="text" path="link" size="100" />
				</li>
				
				<spring:url value="/palaces/${roomEntity.palace.id}/rooms/" var="back_url">
					</spring:url> 
				
				<div class="disponibilidad_botonera">	
				<a class="button" href="${back_url}">Volver</a >
				<input type="submit" class="button" style="width:auto!important;" title="Editar" />
				</div>
				
				</ul>
				
				

</section>
				
			</li>
		</ul>
	</form:form>


<!-- <div id="ok_ingreso" style="display:none;"> -->
<!--    Su reserva se ha recibido exitosamente.  Recibirá en su correo electrónico el código con el que puede revisar el estado de su reserva, modificarla o eliminarla.  Además, ese mismo código y su documento de identificación, serán los datos necesarios para hacer check in cuando ha reservado.  ¡Bienvenido a Coñaripe! -->
<!-- </div> -->
      
</section>

