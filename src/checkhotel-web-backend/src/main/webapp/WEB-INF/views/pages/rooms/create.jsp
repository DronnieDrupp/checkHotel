<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont" style="height:auto;">
	<h3>Ingreso nueva Caba�a</h3>
</div>

<section id="mainRight">
<div class="main_espacio"><blockquote>Recuerda, antes de ingresar una nueva caba�a debes asegurarte que ya existe el proveedor.  
Si no existe, ingr�salo antes de registrar una nueva propiedad.</blockquote>
</div> 
		<ul class="disponibilidad_lista">
			<li class="one_col">
				<section></section>
				
				<section>
					<form:form action="save" method="POST" modelAttribute="roomEntity">
						<ul class="reserva">
							<li>
								<label>Nombre</label>
								<form:input type="text" path="description" size="12" />
								<chforms:fieldError modelAttribute="roomEntity" path="description"/>
							</li>
							<li>
								<label>Direcci�n</label>
								<form:input type="text" path="address" size="12" />
								<chforms:fieldError modelAttribute="roomEntity" path="address"/>
							</li>
							<li class="ancho32">
								<label style="font-size:1em;">Tipo de Habitaci�n</label>
									
								<form:select path="type">
								    <form:option value="0" label="Selecciona" />
								    <form:options items="${listType}" itemValue="id" itemLabel="description"/>
								</form:select>
								<chforms:fieldError modelAttribute="roomEntity" path="type"/>
								 
							</li>
							<li class="ancho32">
								<label>Valor</label>
								<form:input type="number" path="price" size="12" />
								<chforms:fieldError modelAttribute="roomEntity" path="price"/>
							</li>
							<li class="ancho32">
								<label>Capacidad</label>
								<form:input type="number" path="numberPerson" size="12" />
								<chforms:fieldError modelAttribute="roomEntity" path="numberPerson"/>
							</li>
							<li>
								<label>Caracteristicas <span>(Escriba las caracteristicas separadas por una ",")</span></label>
								<form:textarea rows="5" cols="100" path="properties"></form:textarea>
							</li>
							<li>
								<label>Comentarios</label>
								<form:textarea rows="5" cols="100" path="comment"></form:textarea>
								<chforms:fieldError modelAttribute="roomEntity" path="comment"/>
							</li>
							<li>
								<label>Link Galer�a</label>
								<form:input type="text" path="link" size="12" />
							</li>
							<div class="disponibilidad_botonera">
								<spring:url value="/palaces/${roomEntity.palace.id}/rooms/" var="back_url"/>
						
									<a class="button" href="${back_url}">Volver</a >
									<input type="submit" style="width:auto!important;" class="button" title="Crear" />
							</div>

						</ul>
					</form:form>
				</section>
			</li>
		</ul>
	

</section>

