<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont" style="height: auto;">
	<h3>Ingresar Nuevo Hotel</h3>
</div>

<section id="mainRight">

	<div class="main_espacio">
		<blockquote>Recuerda, antes de ingresar un nuevo hotel
			debes asegurarte que ya existe el proveedor. Si no existe,
			ingr&aacute;salo antes de registrar una nueva propiedad.</blockquote>
	</div>

	<ul class="disponibilidad_lista">
		<li class="one_col">
			<h3>REGISTRO</h3>
			<section></section>

			<section>
				<spring:url value="/palaces/${id}" var="updateUrl" />
				<form:form action="${updateUrl}" method="PUT"
					modelAttribute="palaceEntity">

					<ul class="reserva">
						<li><label>Nombre del Hotel:</label> <form:input type="text"
								path="description" size="12" /> <chforms:fieldError
								modelAttribute="palaceEntity" path="description" /></li>

						<li><label>Responsable:</label> <form:input type="text"
								path="manager" size="100" /> <chforms:fieldError
								modelAttribute="palaceEntity" path="manager" /></li>

						<li><label>Direcci&oacute;n:</label> <form:input type="text"
								path="address" size="100" /></li>

						<li class="ancho32"><label>Comuna:</label> <form:select
								id="selectmenu3" path="district.id">
								<form:options items="${comunas}" itemLabel="description"
									itemValue="id"></form:options>
							</form:select></li>

						<li class="ancho32"><label>Tel&eacute;fono:</label> <form:input
								type="text" path="phone" size="10" /> <chforms:fieldError
								modelAttribute="palaceEntity" path="phone" /></li>


						<li class="ancho32"><label>Estrellas</label> <form:select
								id="selectmenu3" path="starsNumber">
								<option ${palace.starsNumber == 1 ? 'selected':''}>1</option>
								<option ${palace.starsNumber == 2 ? 'selected':''}>2</option>
								<option ${palace.starsNumber == 3 ? 'selected':''}>3</option>
								<option ${palace.starsNumber == 4 ? 'selected':''}>4</option>
								<option ${palace.starsNumber == 5 ? 'selected':''}>5</option>
							</form:select></li>

						<li class="ancho32"><label>Tipo Hospedaje:</label> <form:select
								id="selectmenu3" path="type">
								<form:options items="${tipoHospedaje}" itemLabel="description"
									itemValue="id"></form:options>
							</form:select></li>
						<li><label>Caracteristicas (separar cada una con
								comas ","):</label> <form:textarea rows="5" cols="100" path="properties"></form:textarea>
						</li>

						<li><label>Comentarios:</label> <form:textarea rows="5"
								cols="100" path="comment"></form:textarea> <chforms:fieldError
								modelAttribute="palaceEntity" path="comment" /></li>

						<div class="disponibilidad_botonera">
							<input type="submit" class="button" name="Guardar" />
						</div>

					</ul>

				</form:form>
			</section>
		</li>
	</ul>
</section>

