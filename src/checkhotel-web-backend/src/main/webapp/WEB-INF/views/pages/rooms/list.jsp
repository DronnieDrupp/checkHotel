<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<div class="reserva_cont">
	<h3>Cabañas</h3>
</div>

<section id="mainRight">
<div class="main_espacio"><blockquote></blockquote></div> 

<form:form method="POST" modelAttribute="palace">
<h2 style="margin-top:20px;">Centro de Alojamiento: ${palace.description}</h2>
</form:form>
	

	
	<h2 style="margin-top:20px;">Caba&ntilde;as Ingresadas</h2>
	<ul class="disponibilidad_lista">
	 	<c:forEach items="${rooms.content}" var="room" varStatus="loopStatus">
 			<li class="one_col">
		 		<h2>${room.description} (${room.numberPerson} personas) $<fmt:formatNumber value="${room.price}" type="number"/></h2>
		 		<section></section>
			 	<section>
			 		<p><strong>Dirección:</strong>${room.address}</p>
			 		<p><strong>Característica:</strong> ${room.comment}</p>
			 		
			 		<ul>
				 		<c:forEach var="prop" items="${room.listProperties}">
				 		<li>&raquo; ${prop}</li>
				 		</c:forEach>
			 		</ul>
			 		<div class="fotoframe">${room.link}</div>
					<div class="disponibilidad_botonera">
			 		<spring:url value="/palaces/${palaceId}/rooms/${room.id}/update" var="update_form_url">
						<spring:param name="form" />
					</spring:url> 
					<spring:url value="/palaces/${palaceId}/rooms/${room.id}/delete" var="delete_form_url" />
					<form:form action="${delete_form_url}" method="DELETE" >
					<input class="button" type="submit" value="Eliminar" style="width:auto!important;"  class="delete-button"/>
					</form:form>
					
			 		<a href="${update_form_url}" class="button">Modificar</a>
			 		</div>
		 		</section>
		 		
	 		</li>
	 		
			</c:forEach>
	</ul>


<form:form action="add" method="POST" modelAttribute="palaceId">
<h2 style="margin-bottom:20px;">Ingresar Nueva Caba&ntilde;a <a href="${pageContext.request.contextPath}/palaces/${palaceId}/rooms/add" class="button">Nueva Caba&ntilde;a</a></h2> 
</form:form>
			<util:pagination thispage="${rooms}"></util:pagination>

</section>


<script type="text/javascript">
	$(document).ready(function() {
	   $(".img-responsive").remove();
	 });
 </script>