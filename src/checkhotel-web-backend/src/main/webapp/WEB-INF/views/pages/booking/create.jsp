<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont">
	<h3>RESERVA</h3>
</div>
<section id="mainRight">
	<div class="mainEspacio" ></div> 
	<blockquote>Aqu� puedes revisar tu reserva, editarla o eliminarla.  La acci�n de eliminaci�n no puede deshacerse.</blockquote>

		<ul class="disponibilidad_lista">
			<li class="one_col">
					
				<form:form action="save" method="POST" modelAttribute="bookingEntity">
					<h2>${bookingEntity.roomEntity.description} (${bookingEntity.roomEntity.numberPerson} personas)</h2>
					<section></section>
		 			<section>
		 			<h3 class="estadia">
		 			Valor por d�a <strong>: $<fmt:formatNumber value="${bookingEntity.roomEntity.price}" type="number"/></strong></br>
		 			Total estadia completa: <strong>$<fmt:formatNumber value="${bookingEntity.totalBook}" type="number"/></strong>
		 			</h3>
					
					<ul>
					<h2>DETALLES DEL TITULAR</h2>
								<li class="ancho48"><label>Nombre:</label>
								<form:input path="guestEntity.active" type="hidden" value="true"/>
								<form:input path="guestEntity.firstName" type="text" size="40" /> </li>
								<chforms:fieldError modelAttribute="bookingEntity" path="guestEntity.firstName" />
								<li class="ancho48"><label>Apellido:</label>
								<form:input path="guestEntity.lastName" type="text" size="40" /> 
								<chforms:fieldError modelAttribute="bookingEntity" path="guestEntity.lastName"/></li>
								
								<li class="ancho48">
									<label>Rut o Identificaci�n <span style="line-height: 14px;">(sin puntos ni guiones):</span></label> 
									<form:input path="guestEntity.legalId" type="text" size="12" />
									<chforms:fieldError modelAttribute="bookingEntity" path="guestEntity.legalId" />
								</li>
								<li class="ancho48">
									<label>Email:</label>
									<form:input path="guestEntity.email" type="text" size="40" />
									<chforms:fieldError modelAttribute="bookingEntity" path="guestEntity.email" />
								</li>
								<li class="ancho48"><label>Pais: </label>
									<form:select id="selectmenu3" path="guestEntity.country">
									<form:option value="0" label="Selecciona" />
									<form:options items="${countries}" itemLabel="description" itemValue="id"></form:options>
									</form:select>
									<chforms:fieldError modelAttribute="bookingEntity" path="guestEntity.country" />
								</li>
							</ul>

							
							<ul>
							<h2>DETALLES DE LA RESERVA</h2>
								
								<li class="ancho48">
<!-- 								Estos dos input se tiene que ver como texto -->
									<label>Check in:</label>
									<fmt:formatDate	var='formattedDateFrom' value='${bookingEntity.dateFrom}' pattern="dd-MM-yyyy" /> 
									<form:input path="dateFrom" type="text" size="10" class="input_limpio" readonly="true" value="${dateFromB}" />
				 			
								</li>
								<li class="ancho48">
									<label>Check out:</label>
									<fmt:formatDate	var='formattedDateTo' value='${bookingEntity.dateTo}' pattern="dd-MM-yyyy" /> 
									<form:input path="dateTo" type="text" size="10" class="input_limpio" readonly="true" value="${dateToB}" />
								</li>
							</ul>
								<spring:url value="${formattedDateFrom}" var="dateFromB"/>
								<spring:url value="${formattedDateTo}" var="dateToB"/>
								<spring:url value="${bookingEntity.numberSearch}" var="num"/>
							<div class="fotoframe">${bookingEntity.roomEntity.link}</div>

					</section>
					
					
					
					
					<div class="disponibilidad_botonera">
						
						<form:input path="roomEntity.id" type="hidden" size="12" value="${bookingEntity.roomEntity.id}" />
						<button type="submit" >Reservar</button>
				</form:form>	
					 
						<spring:url value="/bookings/Rooms" var="back_url" />
						<form:form action="${back_url}" method="POST" modelAttribute="objectSearch">
						
							
							
				 			<form:input path="dateFrom" type="hidden" size="10" value="${dateFromB}" />
				 			
				 			
				 			<form:input path="dateTo" type="hidden" size="10" value="${dateToB}" /> 
				 			<form:input path="numberPerson" type="hidden" size="10" value="${num}" /> 
			 			
<%-- 							<a class="button" href="${back_url}">Volver</a > --%>
							
							<button type="submit" >Volver</button>
						</form:form>
					</div>
				
			</li>
		</ul>       
		   
</section>