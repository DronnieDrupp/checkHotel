<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<spring:message var="title" code="error_default_title" htmlEscape="false"/>
<spring:message var ="description" code="error_default_problemdescription" htmlEscape="false"/>
<div class="reserva_cont">
	<h3>Error</h3>
</div>
<section id="mainRight">
	<div class="foto_intro"></div>
	<div class="mainEspacio" ></div> 
	<blockquote></blockquote>
	<blockquote></blockquote>
		
	<h2>${title}</h2>

	<p><strong>${fn:escapeXml(description)}</strong></p>

	<p>Requested URL= ${url}</p>
	<p> Exception= ${exception.message}</p>
	<p> <strong>Exception Stack Trace</strong></p>
	<c:forEach items="${exception.stackTrace}" var="ste">
		${ste}
	</c:forEach>
</section>	


