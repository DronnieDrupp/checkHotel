package cl.checkhotel.domain.basicEnum;

public enum Status {
	
	ACTIVE ("ACT","Activa"),
    CANCEL("CAN","Cancelada"),
    USING ("UNS","En Uso"),
    DELETE ("ELI","Eliminada"),
    PAID("PAG","Pagada");
    
    private String id;
	private String desc;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	private Status(String id, String desc) {
		this.id = id;
		this.desc = desc;
	}
	
	
	
	

}
