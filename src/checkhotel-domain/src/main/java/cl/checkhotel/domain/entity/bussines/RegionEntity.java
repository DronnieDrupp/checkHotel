package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import cl.checkhotel.domain.entity.based.BasedEntity;

@Entity
@Table(name = "region")
public class RegionEntity extends BasedEntity {
	
	@ManyToOne
	@JoinColumn(columnDefinition="integer", name="pais")	
	private CountryEntity country;

	public CountryEntity getCountry() {
		return country;
	}

	public void setCountry(CountryEntity country) {
		this.country = country;
	}
	
	
	

}
