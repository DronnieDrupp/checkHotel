package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotBlank;

import cl.checkhotel.domain.entity.based.BasedAdm;

@Entity
@Table(name = "centros")
@Where(clause="activo=1")
public class PalaceEntity extends BasedAdm{

	@NotNull
	@Column(name = "cliente")
	private Long client;	

	@ManyToOne(optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "comuna")
	private DistrictEntity district;
	
	@NotNull(message="{cl.checkhotel.domain.entity.based.PalaceEntity.phone.notNull}")
	@Column(name = "telefono")
	private Integer phone;	
	
	@Column(name = "estrellas", nullable=true)
	private Integer starsNumber;	
	
	@NotNull(message="{cl.checkhotel.domain.entity.based.PalaceEntity.manager.notNull}")
	@NotBlank(message="{cl.checkhotel.domain.entity.based.PalaceEntity.manager.notBlank}")
	@Column(name = "responsable")
	private String manager;	

	public Long getClient() {
		return client;
	}

	public void setClient(Long client) {
		this.client = client;
	}

	public Integer getPhone() {
		return phone;
	}

	public void setPhone(Integer phone) {
		this.phone = phone;
	}

	public Integer getStarsNumber() {
		return starsNumber;
	}

	public void setStarsNumber(Integer starsNumber) {
		this.starsNumber = starsNumber;
	}

	public String getManager() {
		return manager;
	}
	
	public void setManager(String manager) {
		this.manager = manager;
	}	


	/**
	 * @return the district
	 */
	public DistrictEntity getDistrict() {
		return district;
	}

	/**
	 * @param district the district to set
	 */
	public void setDistrict(DistrictEntity district) {
		this.district = district;
	}
	
	
}
