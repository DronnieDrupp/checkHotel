package cl.checkhotel.domain.helper;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

public class ValidationHelper {

	/**
	 * Realiza las validaciones
	 * 
	 * @param validator
	 *            El validador responsable de ejecutar las validaciones.
	 * @param target
	 *            El objeto a validar.
	 * @param groups
	 *            grupos por validar
	 */
	public static <T> void throwExceptionIfObjectIsNotValid(
			Validator validator, T target, Class<?>... groups)
			throws javax.validation.ConstraintViolationException {
		Set<ConstraintViolation<T>> violations = validator.validate(
				target, groups);

		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
	}
}
