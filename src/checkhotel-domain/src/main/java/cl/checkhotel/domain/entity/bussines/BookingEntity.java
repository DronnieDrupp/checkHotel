package cl.checkhotel.domain.entity.bussines;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import cl.checkhotel.domain.base.validation.constraints.Range;
import cl.checkhotel.domain.base.validation.constraints.ReservationAvailability;
import cl.checkhotel.domain.base.validation.groups.BookingUpdate;
import cl.checkhotel.domain.entity.based.BasedId;

@Entity
@Table(name = "reserva")
@Where(clause = "activo=1")
@ReservationAvailability
@Range
public class BookingEntity extends BasedId<Long> {

	@Column(name = "estado", nullable = false)
	@NotEmpty
	private String status;

	@Column(name = "fechaTermino", nullable = false)
	@NotNull
	@DateTimeFormat(pattern="dd-MM-yyyy")
	private Date dateTo;

	@Column(name = "fechaInicio", nullable = false)
	@NotNull
	@DateTimeFormat(pattern="dd-MM-yyyy")
	private Date dateFrom;

	@Transient
	private Collection<Long> listRooms;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "pasajero")
	@Valid
	private GuestEntity guestEntity;

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "habitacion")
	private RoomEntity roomEntity;

	@Override
	@NotNull(groups = BookingUpdate.class)
	public Long getId() {
		return super.getId();
	}
	
	@Transient
	public BigDecimal totalBook;

	@Transient
	public Integer numberSearch;
	/**
	 * Obtiene el estado actual de la reserva
	 */
	public String getStatus() {
		
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the dateTo
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * @param dateTo
	 *            the dateTo to set
	 */
	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	/**
	 * @return the dateFrom
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Collection<Long> getListRooms() {
		return listRooms;
	}

	public void setListRooms(Collection<Long> listRooms) {
		this.listRooms = listRooms;
	}

	public GuestEntity getGuestEntity() {
		return guestEntity;
	}

	public void setGuestEntity(GuestEntity guestEntity) {
		this.guestEntity = guestEntity;
	}

	public void setTotalBook(BigDecimal totalBook) {
		this.totalBook = totalBook;
	}

	public RoomEntity getRoomEntity() {
		return roomEntity;
	}

	public void setRoomEntity(RoomEntity roomEntity) {
		this.roomEntity = roomEntity;
	}

	public BigDecimal getTotalBook() {
		Double days = Math.floor( (dateTo.getTime() - dateFrom.getTime())  / (1000 * 60 * 60 * 24));
		
		BigDecimal period = new BigDecimal(days);
		
		return roomEntity.getPrice().multiply(period);
	}


	public Integer getNumberSearch() {
		return numberSearch;
	}

	public void setNumberSearch(Integer numberSearch) {
		this.numberSearch = numberSearch;
	}
	

}
