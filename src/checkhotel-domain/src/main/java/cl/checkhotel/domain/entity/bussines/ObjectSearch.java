package cl.checkhotel.domain.entity.bussines;

import java.util.Date;

import javax.validation.constraints.NotNull;

public class ObjectSearch {

	@NotNull(message="{cl.checkhotel.domain.entity.based.ObjectSearch.dateTo.notNull}")
	private Date dateTo;

	@NotNull(message="{cl.checkhotel.domain.entity.based.ObjectSearch.dateFrom.notNull}")
	private Date dateFrom;

	@NotNull(message="{cl.checkhotel.domain.entity.based.ObjectSearch.numberPerson.notNull}")
	private Integer numberPerson;

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Integer getNumberPerson() {
		return numberPerson;
	}

	public void setNumberPerson(Integer numberPerson) {
		this.numberPerson = numberPerson;
	}

}
