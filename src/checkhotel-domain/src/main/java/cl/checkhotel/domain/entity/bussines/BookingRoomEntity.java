package cl.checkhotel.domain.entity.bussines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "reservaHabitacion")
public class BookingRoomEntity implements Serializable  {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 8246766081367156954L;

	@Id
	@Column(name = "idReserva")
	private Long idBooking;
	
	@Column(name = "idHabitacion")
	private Long idRoom;

	public Long getIdBooking() {
		return idBooking;
	}

	public void setIdBooking(Long idBooking) {
		this.idBooking = idBooking;
	}

	public Long getIdRoom() {
		return idRoom;
	}

	public void setIdRoom(Long idRoom) {
		this.idRoom = idRoom;
	}
	
	
}
