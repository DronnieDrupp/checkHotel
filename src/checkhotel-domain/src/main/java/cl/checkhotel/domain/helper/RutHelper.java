package cl.checkhotel.domain.helper;

import org.apache.commons.lang3.StringUtils;

/**
 * Helper para RUTs.
 * 
 */
public final class RutHelper {

	private static final int MAX_LENGHT_RUT = 9;

	private RutHelper() {
	}

	/**
	 * Elimina cualquier carácter que no sea número o K al final de la cadena
	 * especificada.
	 * 
	 * @param rut
	 *            La cadena a limpiar.
	 */
	public static String clear(String rut) {
		if (StringUtils.isEmpty(rut))
			return rut;

		StringBuilder builder = new StringBuilder();

		rut = rut.trim();
		boolean notZeroFound = false;

		for (int i = 0; i < rut.length(); i++) {
			char c = rut.charAt(i);

			if (Character.isDigit(c) || c == 'k' || c == 'K') {
				if (c != '0' || notZeroFound) {
					builder.append(c);
					notZeroFound = true;
				}
			}
		}

		return builder.toString().toUpperCase();
	}

	/**
	 * Obtiene un rut formateado con puntos y guión.
	 * 
	 * @param input
	 *            rut a formatear.
	 */
	public static String format(String input) {
		return format(input, true);
	}

	/**
	 * Obtiene el rut formateado sin el digito verificador.
	 * 
	 * @param rut
	 *            El rut a formatear.
	 */
	public static String formatWithoutCheckDigit(String rut) {
		return format(rut, false);
	}

	/**
	 * Obtiene un valor que indica si el rut especificado es válido.
	 * 
	 * @param rut
	 *            El rut a validar.
	 * @return true si es válido. false en caso contrario.
	 */
	public static boolean isValid(String rut) {
		if (StringUtils.isBlank(rut))
			return false;

		if (rut.length() == 1)
			return false;

		char dv = Character.toUpperCase(rut.charAt(rut.length() - 1));

		String cleanRut = clear(rut);
		cleanRut = cleanRut.substring(0, cleanRut.length() - 1);

		if (cleanRut.length() > MAX_LENGHT_RUT)
			return false;

		char checkDigit = getCheckDigit(Integer.parseInt(cleanRut));

		if (checkDigit == dv)
			return true;
		else
			return false;
	}

	private static String format(String input, boolean includeCheckDigit) {
		if (StringUtils.isBlank(input))
			return input;

		String rut = clear(input);

		if (StringUtils.isBlank(rut))
			return rut;

		StringBuilder builder = new StringBuilder();

		String numberPart = rut.substring(0, rut.length() - 1);

		if (numberPart.length() > 3) {
			int i;

			for (i = numberPart.length() - 3; i >= 0; i = i - 3) {
				builder.insert(0, numberPart.substring(i, i + 3));
				builder.insert(0, ".");
			}

			if (i < 0)
				builder.insert(0, numberPart.substring(0, 3 + i));

			if (builder.charAt(0) == '.')
				builder.substring(0, 1);
		} else {
			builder.append(numberPart);
		}

		if (includeCheckDigit) {
			char dv = Character.toUpperCase(rut.charAt(rut.length() - 1));

			builder.append("-");
			builder.append(dv);
		}

		return builder.toString();
	}

	private static char getCheckDigit(int rut) {
		int digit;
		int count;
		int multiply;
		int acumulator;

		count = 2;
		acumulator = 0;

		while (rut != 0) {
			multiply = (rut % 10) * count;
			acumulator = acumulator + multiply;
			rut = rut / 10;
			count = count + 1;
			if (count == 8)
				count = 2;
		}

		digit = 11 - (acumulator % 11);

		if (digit == 10)
			return 'K';
		if (digit == 11)
			return '0';

		return String.valueOf(digit).charAt(0);
	}
}
