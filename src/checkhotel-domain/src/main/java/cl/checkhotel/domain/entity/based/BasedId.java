package cl.checkhotel.domain.entity.based;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public class BasedId<T> {
	
	@Id
	@GeneratedValue
	private T id;
	
	@Column(name="activo")
	@NotNull
	private Boolean active;

	/**
	 * Obtiene el id del objeto
	 */
	public T getId() {
		return id;
	}

	/**
	 * setea el id de los objetos
	 */
	public void setId(T id) {
		this.id = id;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
}
