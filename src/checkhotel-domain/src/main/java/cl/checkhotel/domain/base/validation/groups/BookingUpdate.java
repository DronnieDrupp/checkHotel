package cl.checkhotel.domain.base.validation.groups;

import javax.validation.groups.Default;

/**
 * Interfaz para identificar las validaciones que solo aplican sobre la
 * operacion de actualizacion de reservas, al extender de Default permite que al
 * validar se ejecuten las validaciones que no estan asociadas a un grupo
 * 
 * @author ftroncoso
 *
 */
public interface BookingUpdate extends Default {

}
