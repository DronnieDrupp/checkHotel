package cl.checkhotel.domain.entity.based;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.StringUtils;

@MappedSuperclass
public class BasedAdm extends BasedEntity {
	
	@Min(value = 1, message="{cl.checkhotel.domain.entity.based.BasedAdm.type.notNull}")
	@NotNull(message="{cl.checkhotel.domain.entity.based.BasedAdm.type.notNull}")
	@Column(name = "tipo")
	private Long type;
	
	@Column(name = "caracteristicas", nullable=true)
	private String properties;

	@NotNull(message="{cl.checkhotel.domain.entity.based.BasedAdm.comment.notNull}")
	@NotBlank(message="{cl.checkhotel.domain.entity.based.BasedAdm.comment.notBlank}")
	@Column(name = "comentario")
	private String comment;
	
	@NotNull(message="{cl.checkhotel.domain.entity.based.BasedAdm.address.notNull}")
	@NotBlank(message="{cl.checkhotel.domain.entity.based.BasedAdm.address.notBlank}")
	@Column(name = "direccion")
	private String address;	
	
	public Collection<String> getListProperties() {
		
		List<String> newList = new ArrayList<>();

		if (!StringUtils.isEmpty(properties)) {

			for (String prop : properties.split(",")) {

				newList.add(prop);

			}

		}
		
		return newList;
	}


	/**
	 * @return the type
	 */
	public Long getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Long type) {
		this.type = type;
	}

	/**
	 * @return the properties
	 */
	public String getProperties() {
		return properties;
	}

	/**
	 * @param properties the properties to set
	 */
	public void setProperties(String properties) {
		this.properties = properties;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

}
