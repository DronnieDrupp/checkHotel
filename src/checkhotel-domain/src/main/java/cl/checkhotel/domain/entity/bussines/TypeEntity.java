package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cl.checkhotel.domain.entity.based.BasedEntity;

@Entity
@Table(name = "tipoHospedaje")
public class TypeEntity extends BasedEntity {
	
	@Column(name = "indicadorTipo", nullable=false)
	private Integer indicator;

	/**
	 * @return the indicator
	 */
	public Integer getIndicator() {
		return indicator;
	}

	/**
	 * @param indicator the indicator to set
	 */
	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	
}
