package cl.checkhotel.domain.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import cl.checkhotel.domain.base.validation.constraints.Rut;

/**
 * 
 *
 * $Id$ $Revision$
 * 
 * @author ftroncoso
 *
 */
@Entity
@Table(name = "usuario", uniqueConstraints = { @UniqueConstraint(columnNames = "id_legal"),
		@UniqueConstraint(columnNames = "email") })
// @Unique.List({ @Unique(propertyName = "idNumber"),
// @Unique(propertyName = "email") })
// @Where(clause = "activo=1")
public class User {

	public User() {

	}

	@Id
	@GeneratedValue
	@Column()
	private Long id;

	@NotNull
	@Email
	@NotEmpty
	@Size(max = 30)
	@Column(name = "email")
	private String email;

	@NotNull
	@NotEmpty
	@Size(max = 30)
	@Column(name = "nombre")
	private String name;

	@NotNull
	@NotEmpty
	@Size(max = 30)
	@Column(name = "apellidoPaterno")
	private String lastName;

	// @NotNull
	// @NotEmpty
	// @Size(max = 30)
	@Column(name = "apellidoMaterno")
	private String motherName;

	@NotNull
	@NotEmpty
	@Size(max = 50)
	@Column(name = "clave")
	private String password;

	@Rut
	@NotNull
	@Column(name = "id_legal", length = 10)
	private String legalId;

	@Column(name = "rol")
	@NotNull
	@Enumerated(EnumType.STRING)
	private Role role;

	@NotNull
	@Column(name = "cliente")
	private Long client;

	@Column(name = "activo")
	@NotNull
	private Boolean active;

	@Column(name = "cuentaNoBloqueada")
	@NotNull
	private Boolean accountNonLocked;

	@Column(name = "claveNoExpirada")
	@NotNull
	private Boolean credentialsNonExpired;

	@Column(name = "cuentaNoExpirada")
	@NotNull
	private Boolean accountNonExpired;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	public String getLegalId() {
		return legalId;
	}

	public void setLegalId(String legalId) {
		this.legalId = legalId;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the motherName
	 */
	public String getMotherName() {
		return motherName;
	}

	/**
	 * @param motherName
	 *            the motherName to set
	 */
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public String getFullName() {
		return name + " " + lastName + " " + motherName;
	}

	public void setFullName(String fullName) {
	}

	/**
	 * @return the client
	 */
	public Long getClient() {
		return client;
	}

	/**
	 * @param client
	 *            the client to set
	 */
	public void setClient(Long client) {
		this.client = client;
	}

	public Boolean getCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	public void setCredentialsNonExpired(Boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public Boolean getAccountNonExpired() {
		return accountNonExpired;
	}

	public void setAccountNonExpired(Boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public Boolean getAccountNonLocked() {
		return accountNonLocked;
	}

	public void setAccountNonLocked(Boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}
}
