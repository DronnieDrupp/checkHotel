package cl.checkhotel.domain.base.validation.constraints;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {})
public @interface Range {

	String message() default "{cl.checkhotel.domain.base.validation.constraints.Range.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
