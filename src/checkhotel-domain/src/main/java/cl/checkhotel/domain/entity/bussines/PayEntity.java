package cl.checkhotel.domain.entity.bussines;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import cl.checkhotel.domain.entity.based.BasedId;

@Entity
@Table(name = "pago")
public class PayEntity extends BasedId<Long> {
	
	@NotNull(message="{cl.checkhotel.domain.entity.based.PayEntity.voucher.notNull}")
	@NotBlank(message="{cl.checkhotel.domain.entity.based.PayEntity.voucher.notBlank}")
	@Column(name = "numeroComprobante")
	private String voucher;
	
	@Column(name = "montoPago")
	private BigDecimal paymentAmount;
	
	@Column(name = "numeroTarjeta")
	private String numerdCard;
	
	@Column(name = "tipoDePago")
	private String paymentType;
	
	@Column(name = "reserva")
	private Long booking;

	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getNumerdCard() {
		return numerdCard;
	}

	public void setNumerdCard(String numerdCard) {
		this.numerdCard = numerdCard;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Long getBooking() {
		return booking;
	}

	public void setBooking(Long booking) {
		this.booking = booking;
	}

	
}
