package cl.checkhotel.domain.entity.bussines;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Where;

import cl.checkhotel.domain.entity.based.BasedAdm;

@Entity
@Table(name = "habitacion")
@Where(clause="activo=1")
public class RoomEntity extends BasedAdm {

	@NotNull(message="{cl.checkhotel.domain.entity.based.RoomEntity.palace.notNull}")
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@JoinColumn(name = "centro")
	private PalaceEntity palace;

	@NotNull(message="{cl.checkhotel.domain.entity.based.RoomEntity.prince.notNull}")
	@Column(name = "valor")
	private BigDecimal price;

	@NotNull(message="{cl.checkhotel.domain.entity.based.RoomEntity.numberPerson.notNull}")
	@Column(name = "numeroPersonas")
	private Integer numberPerson;

	@Column(name = "linkGaleria", nullable=true)
	private String link;

	/**
	 * @return the palace
	 */
	public PalaceEntity getPalace() {
		return palace;
	}

	/**
	 * @param palace
	 *            the palace to set
	 */
	public void setPalace(PalaceEntity palaceEntity) {
		this.palace = palaceEntity;
	}

	/**
	 * @return the price
	 */
	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            the price to set
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	/**
	 * @return the numberPerson
	 */
	public Integer getNumberPerson() {
		return numberPerson;
	}

	/**
	 * @param numberPerson
	 *            the numberPerson to set
	 */
	public void setNumberPerson(Integer numberPerson) {
		this.numberPerson = numberPerson;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

}
