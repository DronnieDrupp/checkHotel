package cl.checkhotel.domain.entity.bussines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clienteCentros")
public class ClientPalaceEntity implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5086681242306609376L;

	@Id
	@Column(name = "idCliente")
	private Long idClient;
	
	@Column(name = "idCentros")
	private Long idPalace;

	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	public Long getIdPalace() {
		return idPalace;
	}

	public void setIdPalace(Long idPalace) {
		this.idPalace = idPalace;
	}
	
	

}
