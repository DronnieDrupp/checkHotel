package cl.checkhotel.domain.user;

/**
 * Representa un rol para ser asignado a un usuario
 * 
 * @author ftroncoso
 *
 */
public enum Role {
	ADMIN, MANAGER
}