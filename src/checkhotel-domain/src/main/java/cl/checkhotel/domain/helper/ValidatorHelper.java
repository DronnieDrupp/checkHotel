package cl.checkhotel.domain.helper;

public class ValidatorHelper {

	public static void validateIllegalArgument(Object target, String string) {
		if (null == target) {
			throw new IllegalArgumentException(String.format(
					"%s no puede ser null", string));
		}

	}

	public static void validateParameter(Object target,
			RuntimeException exception) {
		if (null == target) {
			throw exception;
		}
	}

}
