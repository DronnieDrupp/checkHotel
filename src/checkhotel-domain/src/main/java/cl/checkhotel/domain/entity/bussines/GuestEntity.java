package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Where;
import org.hibernate.validator.constraints.NotBlank;

import cl.checkhotel.domain.base.validation.groups.GuestUpdate;
import cl.checkhotel.domain.entity.based.BasedId;

@Entity
@Table(name = "pasajero")
@Where(clause = "activo=1")
public class GuestEntity extends BasedId<Long> {

	@NotNull
	@NotBlank
	@Column(name = "id_legal", nullable = false, unique = true)
	private String legalId;

	@NotNull
	@NotBlank
	@Column(name = "nombre", nullable = false)
	private String firstName;

	@NotNull
	@NotBlank
	@Column(name = "apellido", nullable = false)
	private String lastName;

	@NotNull
	@NotBlank
	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "pais")
	private Long country;

	@Override
	@NotNull(groups = GuestUpdate.class)
	public Long getId() {
		return super.getId();
	}

	/**
	 * obtiene el identificador tributario o legal del pasajero (RUT, Pasaporte
	 * u otro)
	 */
	public String getLegalId() {
		return legalId;
	}

	/**
	 * setea el identificador del pasajero
	 */
	public void setLegalId(String legalId) {
		this.legalId = legalId;
	}

	/**
	 * obtiene el nombre del pasajero
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * setea el nombre del pasajero
	 */
	public void setFirstName(String name) {
		this.firstName = name;
	}

	/**
	 * obtiene el apellido del pasajero
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * setea el apellido del pasajero
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * obtiene el correo electronico del pasajero
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * setea el correo electronico del pasajero
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * obtiene el pais del pasajero
	 */
	public Long getCountry() {
		return country;
	}

	/**
	 * setea el pais del pasajero
	 */
	public void setCountry(Long country) {
		this.country = country;
	}

	public String getFullName() {
		return StringUtils.join(new String[] { firstName, lastName }, ' ');
	}

}
