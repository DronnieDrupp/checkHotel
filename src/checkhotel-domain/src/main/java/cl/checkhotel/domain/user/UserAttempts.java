package cl.checkhotel.domain.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "intentosUsuario", uniqueConstraints = { @UniqueConstraint(columnNames = "usuario") })
public class UserAttempts {

	@Id
	@GeneratedValue
	@Column()
	private Long id;

	@Column(name = "usuario")
	private String username;

	@Column(name = "intentos")
	private Integer attempts;

	@Column(name = "ultimaModificacion")
	private Date lastModified;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getAttempts() {
		if (null== attempts){
			attempts = 0;
		}
		return attempts;
	}

	public void setAttempts(Integer attempts) {
		this.attempts = attempts;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

}