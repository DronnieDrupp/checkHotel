package cl.checkhotel.domain.entity.bussines;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import cl.checkhotel.domain.entity.based.BasedEntity;

@Entity
@Table(name = "comuna")
public class DistrictEntity extends BasedEntity{

	@Column(name="region")
	private Long state;

	/**
	 * @return the state
	 */
	public Long getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Long state) {
		this.state = state;
	}
	
	
}
