-- -----------------------------------------------------
-- Table `checkHotel`.`pais`
-- -----------------------------------------------------
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(56,'Chile',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(54,'Argentina',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(591,'Bolivia',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(55,'Brasil',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(57,'Colombia',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(51,'Perú',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(1,'EE.UU',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(52,'México',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(49,'Alemania',true);
INSERT INTO checkHotel.pais  (id,descripcion,activo) values(33,'Francia',true);

-- -----------------------------------------------------
-- Table `checkHotel`.`region`
-- -----------------------------------------------------
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(15,'Arica y Parinacota',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(1,'Tarapacá',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(2,'Antofagasta',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(3,'Atacama',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(4,'Coquimbo',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(5,'Valparaíso',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(13,'(RM) Santiago',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(6,'OHiggins',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(7,'Maule',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(8,'Bío-Bío',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(9,'La Arauncanía',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(14,'Los Ríos',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(10,'Los Lagos',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(11,'Aysén',56,true);
INSERT INTO checkHotel.region  (id, descripcion,pais,activo) values(12,'Magallanes',56,true);

-- -----------------------------------------------------
-- Table `checkHotel`.`comuna`
-- ----------------------------------------------------
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(1,'Corral',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(2,'Lanco',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(3,'Los Lagos',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(4,'Máfil',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(5,'Mariquina',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(6,'Paillaco',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(7,'Panguipulli',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(8,'Valdivia',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(9,'Futrono',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(10,'La Unión',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(11,'Lago Ranco',14,true);
INSERT INTO checkHotel.comuna (id,descripcion,region,activo) values(12,'Río Bueno',14,true);

-- -----------------------------------------------------
-- Table `checkHotel`.`tipoHospedaje`
-- -----------------------------------------------------
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(1,'Habitación Simple',0,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(2,'Habitación doble',0,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(3,'Habitación Triple',0,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(4,'Suite',0,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(5,'Cabaña',0,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(6,'Casa',0,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(7,'ApartHotel',1,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(8,'Hotel',1,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(9,'Motel',1,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(10,'Centro Alojamiento',1,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(11,'Resort',1,true);
INSERT INTO checkHotel.tipoHospedaje  (id,descripcion,indicadorTipo,activo) values(12,'Corretajes',1,true);

-- -----------------------------------------------------
-- Table `checkHotel`.`cliente`
-- -----------------------------------------------------
INSERT INTO checkHotel.cliente (rutRepresentante,direccion,email,descripcion,nombreRep,apellidoRep,sitioWeb,activo) values('103459877','Guido beck de ramberga 246','emial','Corretajes Coñaripe','Cecilia','Villanueva','www.corretaje.cl',true);
INSERT INTO checkHotel.cliente (rutRepresentante,direccion,email,descripcion,nombreRep,apellidoRep,sitioWeb,activo) values('27','Miami 3325','emial','Hotel','Ronald','Fernandez','www.Hotel.cl',true);

-- -----------------------------------------------------
-- Table `checkHotel`.`usuario`
-- -----------------------------------------------------
INSERT INTO checkHotel.usuario (id_legal,clave,cliente,nombre,apellidoPaterno,apellidoMaterno,email,rol,activo,cuentaNoBloqueada,cuentaNoExpirada,claveNoExpirada) values('103459877','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1,'Cecilia','Villanueva','Herrera','corretaje.conaripe@gmail.com','MANAGER',true,true,true,true);
INSERT INTO checkHotel.usuario (id_legal,clave,cliente,nombre,apellidoPaterno,apellidoMaterno,email,rol,activo,cuentaNoBloqueada,cuentaNoExpirada,claveNoExpirada) values('27','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1,'Ronald','Fernandez','Valenzuela','rfernandez.14@gmail.com','ADMIN',true,true,true,true);
INSERT INTO checkHotel.usuario (id_legal,clave,cliente,nombre,apellidoPaterno,apellidoMaterno,email,rol,activo,cuentaNoBloqueada,cuentaNoExpirada,claveNoExpirada) values('36','7110eda4d09e062aa5e4a390b0a572ac0d2c0220',1,'Para','Bloquear','Usuario','rfernandez.14@gmail.com','ADMIN',true,true,true,true);

-- -----------------------------------------------------
-- Table `checkHotel`.`pasajero`
-- -----------------------------------------------------
INSERT INTO checkHotel.pasajero (id_legal,nombre,apellido,email,pais,activo) values('27','Ronald','Fernandez','email',56,true);

-- -----------------------------------------------------
-- Table `checkHotel`.`centros`
-- -----------------------------------------------------
INSERT INTO checkHotel.centros (direccion,descripcion,telefono,estrellas,comuna,responsable,tipo,cliente,caracteristicas,activo,comentario) values('Guido beck de ramberga 246','Corretaje Coñaripe', 974768771,0,7,'Cecilia Villanueva',5,1,'Estacionamiento',true,'Corretajes coñaripe');
INSERT INTO checkHotel.centros (direccion,descripcion,telefono,estrellas,comuna,responsable,tipo,cliente,caracteristicas,activo,comentario) values('Guido beck de ramberga 246','prueba 1', 974768771,0,7,'Robert Landdog',5,1,'Estacionamiento',true,'prueba 1');
INSERT INTO checkHotel.centros (direccion,descripcion,telefono,estrellas,comuna,responsable,tipo,cliente,caracteristicas,activo,comentario) values('Miami 3325','Hotel', 974768772,0,7,'Ronald Fernández',5,2,'Estacionamiento',true,'mi hotel');


-- -----------------------------------------------------
-- Table `checkHotel`.`habitacion`
-- -----------------------------------------------------
INSERT INTO checkHotel.habitacion (centro,direccion,descripcion,tipo,valor,numeroPersonas,linkGaleria,caracteristicas,comentario,activo) values(1,'Av. Los Aleros #2235','Cabaña Angel',12,100000,7,'<iframe src="//embedsocial.com/facebook_album/album_photos/1310904328934301" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>','wifi,2 baños,terraza,piscina,agua caliente','Cabaña super super linda',true);
INSERT INTO checkHotel.habitacion (centro,direccion,descripcion,tipo,valor,numeroPersonas,linkGaleria,caracteristicas,comentario,activo) values(1,'Pasaje Llanquihue #7788','Cabña Central',12,150000,6,'<iframe src="//embedsocial.com/facebook_album/album_photos/1310906365600764" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>','wifi,2 baños,terraza,piscina,agua caliente','Cabaña super super linda',true);
INSERT INTO checkHotel.habitacion (centro,direccion,descripcion,tipo,valor,numeroPersonas,linkGaleria,caracteristicas,comentario,activo) values(2,'Av. Santa Elena #1175','prueba 1',12,100000,8,'facebook.com','wifi','Cabaña',true);


-- -----------------------------------------------------
-- Table `checkHotel`.`reserva`
-- -----------------------------------------------------
INSERT INTO checkHotel.reserva  (fechaInicio,fechaTermino,pasajero,estado,activo,habitacion) values('2016-10-07 15:00:00','2016-10-10 12:00:00',1,'Activa',true,1);
INSERT INTO checkHotel.reserva  (fechaInicio,fechaTermino,pasajero,estado,activo,habitacion) values('2016-10-07 15:00:00','2016-10-10 12:00:00',1,'Activa',true,1);
INSERT INTO checkHotel.reserva  (fechaInicio,fechaTermino,pasajero,estado,activo,habitacion) values('2016-10-07 15:00:00','2016-10-10 12:00:00',1,'Activa',true,1);
INSERT INTO checkHotel.reserva  (fechaInicio,fechaTermino,pasajero,estado,activo,habitacion) values('2016-10-11 15:00:00','2016-10-20 12:00:00',1,'Activa',true,2);
INSERT INTO checkHotel.reserva  (fechaInicio,fechaTermino,pasajero,estado,activo,habitacion) values('2016-10-10 15:00:00','2016-10-24 12:00:00',1,'Activa',true,3);