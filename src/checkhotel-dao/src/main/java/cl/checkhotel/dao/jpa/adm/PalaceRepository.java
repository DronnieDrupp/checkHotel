package cl.checkhotel.dao.jpa.adm;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;

@Repository
public interface PalaceRepository extends PagingAndSortingRepository<PalaceEntity, Long> {
	
	@Query(countQuery = "SELECT count(p) FROM PalaceEntity p where p.client = :clientId", value = "select p from PalaceEntity p where p.client = :clientId")
	Page<PalaceEntity> findAllByClient(Pageable pageable, @Param("clientId") Long clientId);
	
}
