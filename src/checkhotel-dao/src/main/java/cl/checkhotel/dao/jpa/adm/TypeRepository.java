package cl.checkhotel.dao.jpa.adm;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.TypeEntity;

@Repository
public interface TypeRepository extends JpaRepository<TypeEntity, Long> {
	
	@Query("SELECT t FROM TypeEntity t where t.indicator = :indicator")
	Collection<TypeEntity> findByIndicator(@Param("indicator") Integer indicator);

}
