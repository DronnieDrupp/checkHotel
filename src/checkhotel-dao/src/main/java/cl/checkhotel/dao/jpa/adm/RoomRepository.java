package cl.checkhotel.dao.jpa.adm;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.RoomEntity;

@Repository
public interface RoomRepository extends PagingAndSortingRepository<RoomEntity, Long> {


	@Query(countQuery = "SELECT count(r) FROM RoomEntity r where r.palace.id = :palaceId ", value = "SELECT r FROM RoomEntity r where r.palace.id = :palaceId")
	Page<RoomEntity> findByPalace(Pageable pageable, @Param("palaceId") Long palaceId);
	
	@Query(countQuery = "SELECT count(r) FROM RoomEntity r where r.palace.client = :clienId", value = "SELECT r FROM RoomEntity r where r.palace.client = :clienId")
	Page<RoomEntity> findByClient(Pageable pageable, @Param("clienId") Long clienId);
	
	@Query(countQuery = "SELECT count(r) FROM RoomEntity r where r.palace.client = :clienId and r.numberPerson >= :numberPerson", value = "SELECT r FROM RoomEntity r where r.palace.client = :clienId and r.numberPerson >= :numberPerson")
	Page<RoomEntity> findByClientAndNumberPerson(Pageable pageable, @Param("clienId") Long clienId, @Param("numberPerson") Integer numberPerson);

}