package cl.checkhotel.dao.jpa.adm;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.checkhotel.domain.entity.bussines.ClientPalaceEntity;

public interface ClientPalaceRepository extends JpaRepository<ClientPalaceEntity, Long> {

}
