package cl.checkhotel.dao.jpa.guest;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.GuestEntity;
import cl.checkhotel.domain.user.User;

@Repository
public interface GuestRepository extends JpaRepository<GuestEntity, Long> {

	@Query("Select g from GuestEntity g where g.legalId = :legalId")
	GuestEntity findByIdLegal(@Param("legalId") String legalId);

}
