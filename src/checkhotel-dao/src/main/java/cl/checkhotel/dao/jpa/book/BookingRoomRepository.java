package cl.checkhotel.dao.jpa.book;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.checkhotel.domain.entity.bussines.BookingRoomEntity;

public interface BookingRoomRepository extends JpaRepository<BookingRoomEntity, Long> {

}
