package cl.checkhotel.dao.jpa.adm;

import java.util.Collection;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import cl.checkhotel.domain.user.User;

/**
 *
 * $Id$ $Revision$
 * 
 * @author ftroncoso
 *
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	User findByEmail(String email);

	Page<User> findByNameLike(Pageable pageable, String name);

	// @Query("Select u from User u where u.legalId = :legalId")
	// User findByIdNumber(@Param("legalId") String legalId);

	@Query("Select u from User u where u.legalId = :username")
	User findByUserName(@Param("username") String username);

	@Override
	@Query("Select u from User u")
	Collection<User> findAll();

}
