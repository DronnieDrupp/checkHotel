package cl.checkhotel.dao.jpa.book;

import java.util.Collection;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.BookingEntity;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Long> {

	/**
	 * localiza las reservas dentro de un periodo de fechas, en la habitacion
	 * entregada y que el id de reserva no sea el mismo que el entregado (si es
	 * null, es un registro a crear)
	 * 
	 * @param dateFrom
	 *            perdiodo desde
	 * @param dateTo
	 *            periado hasta
	 * @param id
	 *            identificador de reserva (si es una nueva reserva será nulo)
	 * @param roomId
	 *            habitacion consultada su disponibilidad
	 * @return
	 */
	@Query("SELECT b FROM BookingEntity b where b.dateFrom >= :dateFrom and b.dateTo <= :dateTo and b.id <> :id and b.roomEntity.id = :roomId")
	Collection<BookingEntity> findBookingsBeetwenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo,
			@Param("id") Long id, @Param("roomId") Long roomId);
	
	
	
	@Query("SELECT b FROM BookingEntity b where b.dateFrom >= :dateFrom and b.dateTo <= :dateTo and b.roomEntity.id = :roomId")
	Collection<BookingEntity> findBookingsBeetwenDates(@Param("dateFrom") Date dateFrom, @Param("dateTo") Date dateTo, @Param("roomId") Long roomId);

}
