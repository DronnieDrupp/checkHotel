package cl.checkhotel.dao.jpa.book;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cl.checkhotel.domain.entity.bussines.PayEntity;

@Repository
public interface PayRepository extends JpaRepository<PayEntity, Long> {
	
	@Query("SELECT p FROM PayEntity p where p.booking = :bookingId")
	PayEntity findByBooking(@Param("bookingId") Long bookingId);

}
