package cl.checkhotel.dao.jpa.adm;

import cl.checkhotel.domain.user.UserAttempts;

public interface UserDetailsDao {

	void updateFailAttempts(String username);

	void resetFailAttempts(String username);
	
	UserAttempts getUserAttempts(String username);

}