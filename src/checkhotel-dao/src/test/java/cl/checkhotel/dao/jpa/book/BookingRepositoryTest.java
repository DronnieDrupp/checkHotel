package cl.checkhotel.dao.jpa.book;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cl.checkhotel.dao.jpa.adm.PalaceRepository;
import cl.checkhotel.dao.jpa.adm.RoomRepository;
import cl.checkhotel.dao.jpa.guest.GuestRepository;
import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.GuestEntity;
import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.domain.entity.bussines.RoomEntity;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class BookingRepositoryTest extends AbstractJUnit4SpringContextTests {

	private static final String DATE_FROM = "2016-10-07 15:00:00.0";

	private static final String DATE_TO = "2016-10-10 12:00:00.0";

	private SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	private static final String STATUS_BOOK = "ACP";

	private static final String ID_CLIENTE = "19";

	@Autowired
	@Qualifier("bookingRepository")
	private BookingRepository bookingRepository;

	@Autowired
	@Qualifier("palaceRepository")
	private PalaceRepository palaceRepository;
	
	@Autowired
	@Qualifier("roomRepository")
	private RoomRepository roomRepository;

	@Autowired
	@Qualifier("guestRepository")
	private GuestRepository guestRepository;

	private BookingEntity booking;

	private GuestEntity guest;

	private RoomEntity room;

	private PalaceEntity palace;

	@Before
	public void setUp() throws ParseException {
		createGuest();
		createPalace();
		createRoom();
		
		booking = createBooking();
		booking.setGuestEntity(guest);
		booking.setRoomEntity(room);

	}

	private void createPalace() {
		palace = new PalaceEntity();
		palace.setAddress("address");
		palace.setDescription("centro1");
		palace.setPhone(9191947);
		palace.setManager("manager");
		palace.setType(new Long(1));
		palace.setClient(1L);
		palace.setComment("comment");
		palace.setActive(true);
		palaceRepository.save(palace);
	}

	private void createRoom() {
		room = new RoomEntity();
		room.setAddress("address");
		room.setDescription("test");
		room.setPalace(palace);
		room.setActive(Boolean.TRUE);
		room.setType(new Long(5));
		room.setComment("comment");
		room.setNumberPerson(3);
		room.setPrice(new BigDecimal(10000));
		roomRepository.save(room);
	}

	private void createGuest() {
		guest = new GuestEntity();
		guest.setFirstName("juanito");
		guest.setLastName("gonzalez");
		guest.setEmail("a@a.cl");
		guest.setCountry(new Long(56));
		guest.setLegalId(ID_CLIENTE);
		guest.setActive(true);
		
		guestRepository.save(guest);
		guestRepository.flush();
	}

	@After
	public void cleanUp() {

		bookingRepository.deleteAll();
		guestRepository.deleteAll();
		roomRepository.deleteAll();
	}

	private BookingEntity createBooking() throws ParseException {

		booking = new BookingEntity();

		booking.setDateFrom(transforStringDate(DATE_FROM));
		booking.setDateTo(transforStringDate(DATE_TO));
		booking.setStatus(STATUS_BOOK);
		booking.setActive(true);

		return booking;
	}

	private Date transforStringDate(String strDate) throws ParseException {

		Date fecha = null;

		fecha = FORMAT.parse(strDate);

		return fecha;
	}

	@Test
	public void testSavedBooking() {

		bookingRepository.save(booking);
		bookingRepository.flush();

		BookingEntity newBooking = getBookingRecentSave();

		Assert.assertEquals(ID_CLIENTE, newBooking.getGuestEntity().getLegalId());
		Assert.assertEquals(STATUS_BOOK, newBooking.getStatus());
		Assert.assertEquals(DATE_FROM, newBooking.getDateFrom().toString());
		Assert.assertEquals(DATE_TO, newBooking.getDateTo().toString());

	}

	private BookingEntity getBookingRecentSave() {


		BookingEntity book = bookingRepository.findAll().get(0);

		return book;
	}
}
