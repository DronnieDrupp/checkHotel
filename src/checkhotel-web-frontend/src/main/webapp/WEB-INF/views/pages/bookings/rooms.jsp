<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<div class="reserva_cont">
	<h3>Cabañas</h3>
</div>

<section id="mainRight">
<div class="main_espacio hidden"><blockquote></blockquote></div> 
	
	<h2 style="margin-top:20px;">Caba&ntilde;as Ingresadas</h2>
	<ul class="disponibilidad_lista">
	 	<c:forEach items="${rooms.content}" var="room" varStatus="loopStatus">
 			<li class="one_col">
		 		<h2>${room.description} (${room.numberPerson} personas) $<fmt:formatNumber value="${room.price}" type="number"/></h2>
		 		<section></section>
			 	<section>
			 		<p><strong>Dirección:</strong>${room.address}</p>
			 		<p><strong>Característica:</strong> ${room.comment}</p>
			 		
			 		<ul>
				 		<c:forEach var="prop" items="${room.listProperties}">
				 		<li>&raquo; ${prop}</li>
				 		</c:forEach>
			 		</ul>
			 		<div class="fotoframe">${room.link}</div>
		 		</section>
		 		<div class="disponibilidad_botonera">
		 		
			 		<spring:url value="/bookings/add" var="book_form_url"/>
			 		<spring:url value="${room.id}" var="idRoom"/>
			 		
			 		<form:form action="${book_form_url}" method="POST" modelAttribute="bookingEntity">
			 			<fmt:formatDate	var='formattedDateFrom' value='${bookingEntity.dateFrom}' pattern="dd-MM-yyyy" /> 
			 			<form:input path="dateFrom" type="hidden" size="10" value="${formattedDateFrom}" />
			 			
			 			<fmt:formatDate	var='formattedDateTo' value='${bookingEntity.dateTo}' pattern="dd-MM-yyyy" /> 
			 			<form:input path="dateTo" type="hidden" size="10" value="${formattedDateTo}" /> 
			 			<form:input path="numberSearch" type="hidden" size="10" value="${bookingEntity.numberSearch}" /> 
			 			<form:input path="roomEntity.id" type="hidden" size="10" value="${idRoom}" /> 
			 			
						<button type="submit"  class="button" >Reservar</button>
					</form:form>
			 </div>
	 		</li>
	 		
			</c:forEach>
			<div class="disponibilidad_botonera">
		 		
			 		<spring:url value="/bookings" var="back_form_url"/>
					
			 		<a href="${back_form_url}" class="button">Volver</a>
			</div>
	</ul>


			<util:pagination thispage="${rooms}"></util:pagination>

</section>
