<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

	<div class="reserva_cont" style="height: auto!important;">
		<div class="texto_intro">Reserva tu estad�a en los m�s bellos paisajes del Sur de Chile y disfruta de la naturaleza, con la comodidad que mereces</div>
		<h3>Haz tu reserva</h3>
		<spring:url value="/bookings/Rooms" var="updateUrl2" />
		<form:form action="${updateUrl2}" method="POST" modelAttribute="objectSearch">
			<ul class="reserva">
				<li>
					<label>Check in:</label>
					<fmt:formatDate	var='formattedDateFrom' value='${objectSearch.dateFrom}' pattern="dd-MM-yyyy" /> 
					<form:input path="dateFrom" type="text"	size="10" name="datepicker" id="datepicker"	value="${formattedDateFrom}" readonly="true" /> 
					<chforms:fieldError modelAttribute="objectSearch" path="dateFrom" />
				</li>
				<li>
					<label>Check out:</label>
					<fmt:formatDate	var='formattedDateTo' value='${objectSearch.dateTo}' pattern="dd-MM-yyyy" /> 
					<form:input path="dateTo" type="text" size="10" name="datepicker" id="datepicker2" value="${formattedDateTo}"  readonly="true" /> 
					<chforms:fieldError	modelAttribute="objectSearch" path="dateTo" />
				</li>
				<li>
					<label>Hu�spedes:</label>  
					<form:input path="numberPerson" type="number" size="10" style="background:#fff!important;"/> 
					<chforms:fieldError	modelAttribute="objectSearch" path="numberPerson" />
				</li>
				<li>
					<input type="hidden" value="index" name="url" />
					<button type="submit" >Ver Disponibilidad</button>
				</li>	
			</ul>
		</form:form>
	</div>
<section id="mainRight">
	<div class="foto_intro">
		<img src="${pageContext.request.contextPath}/resources/img/foto01.jpg" alt="" title="" border="0" />
	</div>

    <blockquote>Somos una empresa con m�s de 15 a�os de experiencia en convertir tus vacaciones en un paseo inolvidable.  Tenemos a su disposici�n diferetes alternativas, de acuerdo a sus necesidades.  Todos los d�as del a�o.</blockquote>

    <p>En Corretajes Co�aripe, nos dedicamos a la renta de propiedades para fines tur�sticos.  Contamos con diferentes alternativas, pensando en una escapada rom�ntica, turismo aventura con los amigos, un hermoso paseo familiar o convenciones y seminarios empresariales.</p>
    <p>Todo lo que necesites para asegurar unos d�as perfectos, lo puedes encontrar con nosotros.  Haz tu reserva y asegura tu estad�a en un entorno privilegiado, como s�lo el Sur de Chile te puede ofrecer.</p>
    <h2>�BIENVENIDOS A CO�ARIPE!</h2>        
</section>
<script type="text/javascript">
	$(document).ready(function() {
	   $("#datepicker").datepicker({ minDate: 0 });
	   $("#datepicker2").datepicker({ minDate: 0 });
	 });
 </script>