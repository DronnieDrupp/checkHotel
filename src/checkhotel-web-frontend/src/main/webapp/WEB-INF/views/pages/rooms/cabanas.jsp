<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="reserva_cont">
	<h1><a href="${pageContext.request.contextPath}/">Corretajes Co�aripe</a></h1>
	<h3>Nuestras Caba�as</h3>
	<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a nunc vitae enim blandit egestas at luctus ligula. Nullam rutrum egestas sem, sit amet fringilla lectus mollis non</div>
</div>
<img src="${pageContext.request.contextPath}/resources/img/foto04.jpg" alt="">
	<section id="mainRight">
		<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exertion ullamco laboris nisi ut aliquip ex ea commodo consequat.</blockquote>
		
		
	<ul class="reserva">
	
	<li>
		<label>Centros:</label>
			<form:form action="" method="POST"  modelAttribute="palace">  
			<form:select id="palaces"  path="description">
		    	<form:option value="NONE"> --Selecciona--</form:option>
		    	<form:options items="${palaceList}"></form:options>
		    </form:select>
		    </form:form>
	</li>
	</ul>	
		<div id="cabanas"></div>
		
	</section>
	
	<script type= "text/javascript">

    $( document ).ready(function() {


    	
    	$("select#palaces").change(function() {
    	    var val = $("#palaces").val();
    	    
    	    $( "#cabanas" ).empty();
    	    
    	    console.log(val);
    	    
    	    $.ajax({
    	    url : 'rooms/getRoomForPalace',
    	    method : 'POST',
    	    dataType: "json",
    	     data :{
    	    	 idPalace : val
    	            },

    	      success: function (data) {
    	      console.log("Success Response"+ data);
    	      $.each(data, function(i, item) {
    	    	    console.log(item);
    	    	    var divHTML = "<div id='rowCabana'><div class='colLeft'><h2>"+
    	    	    			item.description + " " + "$" + item.price + "</h2>" +
   								"<p>numero de personas: " + item.numberPerson + "</p><p>" + 
   								item.comment + "</p><p><ul>";
   								
   					var prop = item.propertys.split(",");
   					$.each(prop, function(i, item) {
   						divHTML = divHTML +"<li>" + item+ "</li>"
   					});
   								
   					divHTML = divHTML + "</ul></p></div><div class='colRight'>" +
   					"<iframe src='//embedsocial.com/facebook_album/album_photos/10211235308698900' width='900' height='1500' frameborder='0' scrolling='no' marginheight='0' marginwidth='0'></iframe>" + "</div></div>";
    	    					
    	    					
    	    					
    	    					
    	    	    $("#cabanas").append(divHTML);
    	    	});
    	      
    	      

    	      },

    	       error :function()
    	       {
    	    	  var divHTML = "<div class='error'>No hay Caba�as asociadas al centro u hotel seleccionado</div>";
    	    	  $("#cabanas").append(divHTML);
    	        }          

    	       });
    	});
    	
    });
    
    
    


</script>