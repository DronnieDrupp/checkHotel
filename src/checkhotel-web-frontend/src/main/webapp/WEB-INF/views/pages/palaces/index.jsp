<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<div class="reserva_cont">
	<h3>Centros de Hospedajes</h3>
</div>

<section id="mainRight">
<div class="foto_intro"><img src="${pageContext.request.contextPath}/resources/img/foto03.jpg" alt="" title="" border="0" /></div>		
<div class="main_espacio" ></div> 
			
<ul class="disponibilidad_lista">
<c:forEach items="${palaces.content}" var="palace" varStatus="loopStatus">
	<li class="one_col">
		<h3>${palace.description}</h3>
	 	<section></section>
	 				
	 	<section>
	 	<h4>${palace.address} - ${palace.district.description}</h4>
	 	<p>${palace.comment}</p>
	 	
	 	<ul>
		<c:forEach var="prop" items="${palace.listProperties}">
		<li>&raquo; ${prop}</li>
		</c:forEach>
		</ul>
		
	 	<p><strong>Responsable:</strong> ${palace.manager}</p>
	 	<p><strong>Tel&eacute;fono:</strong> ${palace.phone}</p>
	 	</section>
		
		<div class="disponibilidad_botonera">
		<spring:url value="/palaces/${palace.id}/rooms" var="palaceUrl" />
		<button class="button" onclick="location.href='${palaceUrl}'">Caba�as</button>
		</div>
		
	 	</li>
	 	</c:forEach>
	 	</ul>
	 	<util:pagination thispage="${rooms}"></util:pagination>

</section>
