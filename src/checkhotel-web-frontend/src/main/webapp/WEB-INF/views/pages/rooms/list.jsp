<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<div class="reserva_cont" style="height: auto!important;">
	<h3>Nuestras Caba�as</h3>
	<div>Contamos con una gran variedad de caba�as, s�lo debes buscar la que se acomode a tus necesidades, para asegurar unas vacaciones so�adas en un entorno maravilloso.</div>
</div>

<section id="mainRight">
	<div class="foto_intro"><img src="${pageContext.request.contextPath}/resources/img/foto04.jpg" alt="">	</div>
	<blockquote>
<div class="main_espacio" >
	<h3>Todas nuestras caba�as cuentan con:</h3>
&raquo;Vajilla &raquo;Microondas &raquo;Refrigerador &raquo;Agua caliente &raquo;Comedor &raquo;Sala de Estar &raquo;Televisi�n satelital &raquo;Calefacci�n a combusti�n lenta &raquo;Parrillas para asados
</blockquote>
	</div> 
		
	<ul class="disponibilidad_lista">
		<c:forEach items="${rooms.content}" var="room" varStatus="loopStatus">
 			<li class="one_col">
		 		<h2>${room.description} (${room.numberPerson} personas) $<fmt:formatNumber value="${room.price}" type="number"/></h2>
		 		<section>
		 			<div>${room.link}</div>
		 		</section>
			 	<section>
			 		<p><strong>Direcci�n:</strong>${room.address}</p>
			 		<p><strong>Caracter�stica:</strong> ${room.comment}</p>
			 		
			 		<ul>
				 		<c:forEach var="prop" items="${room.listProperties}">
				 		<li>&raquo; ${prop}</li>
				 		</c:forEach>
			 		</ul>
			 		<div class="fotoframe">${room.link}</div>
		 		</section>
	 		</li>
			</c:forEach>
	</ul>
		<util:pagination thispage="${rooms}"></util:pagination>	
		<div id="cabanas"></div>
		
	</section>
	
