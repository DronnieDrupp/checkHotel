<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>

<div class="reserva_cont">
	<h1><a href="${pageContext.request.contextPath}/">Corretajes Co�aripe</a></h1>
	<h3>Nuestras Caba�as</h3>
	<div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a nunc vitae enim blandit egestas at luctus ligula. Nullam rutrum egestas sem, sit amet fringilla lectus mollis non</div>
</div>
<img src="${pageContext.request.contextPath}/resources/img/foto04.jpg" alt="">
	<section id="mainRight">
		<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exertion ullamco laboris nisi ut aliquip ex ea commodo consequat.</blockquote>
		
		
	<ul class="reserva">
	
	<li>
		<label>Caba�as del centro:</label>
		<table class="table table-condensed table-bordered">
			<thead>
				<tr>
					<th>descripci�n</th>
					<!-- otros campos -->
					<th><!-- opciones --></th>
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${rooms.content}" var="room" varStatus="loopStatus">
				<tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
					<td><c:out value="${room.description}" /></td>
					<!-- otros campos -->
					<td>
						<spring:url value="/palaces/${palaceId}/rooms/${room.id}" var="update_form_url">
							<spring:param name="form" />
						</spring:url> 
						<spring:url value="/palaces/${palaceId}/rooms/${room.id}" var="delete_form_url" /> 
						 
						<a class="button" href="${update_form_url}">Modificar</a> |
						<form:form action="${delete_form_url}" method="DELETE" >
							<input type="submit" value="Eliminar"  class="button"/>
						</form:form>
					</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
		<util:pagination thispage="${rooms}"></util:pagination>
	</li>
	</ul>	
		<div id="cabanas"></div>
		
	</section>
	
