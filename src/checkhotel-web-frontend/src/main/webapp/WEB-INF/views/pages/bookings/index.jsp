<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="chforms" tagdir="/WEB-INF/tags/forms"%>

<div class="reserva_cont" style="height: auto!important;">
	<h3>Mi Reserva</h3>
	<div>En esta secci&oacute;n puedes realizar una nueva reserva, revisar los datos de la reserva que ya has hecho o modificarla, todo de manera r&aacute;pida y 100% online.</div>
</div> 
		
<section id="mainRight">
	<div class="foto_intro"><img src="${pageContext.request.contextPath}/resources/img/foto03.jpg" alt="" title="" border="0" /></div>
	
	<section>
	<ul class="disponibilidad_lista">
	<h3>Todas nuestras caba�as cuentan con:</h3>
	<ul>
	<li>&raquo; Vajilla</li>
	<li>&raquo; Microondas</li>
	<li>&raquo; Refrigerador</li>
	<li>&raquo; Agua caliente</li>
	<li>&raquo; Comedor</li>
	<li>&raquo; Sala de Estar</li>
	<li>&raquo; Televisi�n satelital</li>
	<li>&raquo; Calefacci�n a combusti�n lenta</li>
	<li>&raquo; Parrillas para asados</li>
	</ul></ul>
	</section>
	
	
	<h2>Haz Tu Reserva</h2>

	
	
	
	<spring:url value="/bookings/Rooms" var="updateUrl2" />
	<form:form action="${updateUrl2}" method="POST" modelAttribute="objectSearch">
		<ul class="reserva">
			<li class="ancho32">
				<label>Check in:</label>
				<fmt:formatDate	var='formattedDateFrom' value='${objectSearch.dateFrom}' pattern="dd-MM-yyyy" /> 
				<form:input path="dateFrom" type="text"	size="10" name="datepicker" id="datepicker"	value="${formattedDateFrom}" readonly="true" /> 
				<chforms:fieldError modelAttribute="objectSearch" path="dateFrom" />
			</li>
			<li class="ancho32">
				<label>Check out:</label>
				<fmt:formatDate	var='formattedDateTo' value='${objectSearch.dateTo}' pattern="dd-MM-yyyy" /> 
				<form:input path="dateTo" type="text" size="10" name="datepicker" id="datepicker2" value="${formattedDateTo}"  readonly="true" /> 
				<chforms:fieldError	modelAttribute="objectSearch" path="dateTo" />
			</li>
			<li class="ancho32">
				<label>Hu�spedes:</label>  
				<form:input path="numberPerson" type="number" size="10"/>
				<chforms:fieldError	modelAttribute="objectSearch" path="numberPerson" /> 
			<li>
				<input type="hidden" value="bookings/index" name="url" />
				<button type="submit" >Ver Disponibilidad</button>
				
			</li>
		</ul>
	</form:form>

	<h2>Revisa o Modifica tu Reserva</h2>
	<form:form action="bookings?find" method="POST" modelAttribute="bookingEntity">
		<spring:hasBindErrors name="booking">
			<c:if test="${errors.globalErrorCount > 0}">
				<div class="error-container">
					<div class="error-description">
				        <ul class="errors ui-state-error-text" >
							<form:errors element="li" delimiter="&lt;/li&gt;&lt;li&gt;" />
						</ul>
					</div>
				</div>
			</c:if>      
		</spring:hasBindErrors>
			
		<ul class="reserva input-clean">
			<li>
				<label>C�digo de Reserva <span style="font-size:0.7em;">(enviado a tu correo electr�nico):</span></label>
				
				<form:input path="id" type="number" size="12" />
				<br>
				
				<spring:hasBindErrors name="bookingEntity">
				<div class="error-container">
					<div class="error-description">
						<form:errors path="id" />
					</div>
				</div>
				</spring:hasBindErrors>
			</li>
	  		<li>
	  			<button type="submit">Revisar Reserva</button>
	  		</li>
		</ul>
	</form:form>

</section>
<script type="text/javascript">
	$(document).ready(function() {
		$("#datepicker").datepicker({ minDate: 0 });
		$("#datepicker2").datepicker({ minDate: 0 });
	 });
 </script>