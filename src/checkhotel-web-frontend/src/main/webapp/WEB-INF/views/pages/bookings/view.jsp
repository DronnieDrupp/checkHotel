<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="reserva_cont">
	<h3>RESERVA</h3>
</div>
<section id="mainRight">
	<div class="mainEspacio" >
	<blockquote>Aqu� puedes revisar tu reserva, editarla o eliminarla.  La acci�n de eliminaci�n no puede deshacerse.</blockquote>
	</div> 
	
		<ul class="disponibilidad_lista">
		<li class="one_col">
		<section></section>
		<section>
		<h3 class="estadia">
		${bookingEntity.roomEntity.description} <strong>${bookingEntity.roomEntity.price}</strong><br>
		ID DE RESERVA: <strong>${itemId}</strong>
		</h3>
		
		<h2 style="margin-top: 20px!important;" >DETALLES DEL TITULAR</h2>
		<ul>
		<li class="ancho32"><label>Nombre:</label> <h5>${bookingEntity.guestEntity.fullName}</h5></li>
		<li class="ancho32"><label>Rut o Identificaci�n</label> <h5>${bookingEntity.guestEntity.legalId}</h5></li>
		<li class="ancho32"><label>Email:</label> <h5>${bookingEntity.guestEntity.email}</h5></li>
		</ul>
		
		<h2 style="margin-top: 20px!important;" >DETALLES DE LA RESERVA</h2>
		<ul>
		<li class="ancho32"><label>Check in:</label> <h5>${bookingEntity.dateFrom}</h5></li>
		<li class="ancho32"><label>Check out:</label> <h5>${bookingEntity.dateTo}</h5></li>
		<li class="ancho32"><label>N�mero de Personas:</label> <h5>${bookingEntity.roomEntity.numberPerson}</h5></li>
		<li>
		<label>Caracter�sticas:</label> 
		<ul>
		<c:forEach var="strProp" items="${bookingEntity.roomEntity.listProperties}"><li>&raquo; ${strProp}</li></c:forEach>
		</ul></li>
		</ul>		
		</section>
		
		<div class="disponibilidad_botonera">
		<spring:url value="/" var="home_url"></spring:url> 
					
					<a class="button" href="${home_url}">Volver</a >
					
					<spring:url value="/bookings/${itemId}" var="update_form_url">
						<spring:param name="form" />
					</spring:url> 
					<a class="button" href="${update_form_url}">Modificar</a> 
					
					<spring:url value="/bookings/${itemId}" var="delete_form_url" /> 
					<form:form action="${delete_form_url}" method="DELETE" >
						<button type="submit"  class="button" >Eliminar Reserva</button>
					</form:form>
		</div>
		
		</li>
		</ul>       
</section>