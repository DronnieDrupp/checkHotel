<div class="reserva_cont" style="height: auto!important;">
		
		<h2></h2>
		<h3>Qui&eacute;nes somos</h3>
		<div>Somos una empresa familiar dedicada al rubro tur�stico desde hace m�s de 15 a�os, con una enorme experiencia en la renta de propiedades en la localidad de Co�aripe, Regi�n de Los R�os.</div>
		</div>
      <section id="mainRight">
		<div class="foto_intro"><img src="${pageContext.request.contextPath}/resources/img/foto02.jpg" alt="" title="" border="0" /></div>
		
		<blockquote>Deja en nuestras manos las complicaciones de buscar d�nde alojar en tus pr�ximos viajes, y descubre la confianza y calidad de los servicios de Corretajes Co�aripe, nuestra experiencia nos respalda.</blockquote>
	<h2>Nuestra Misi�n</h2>
       <p>
       Proveer un servicio de arriendo y alojamiento vacacional, de calidad y confianza insuperables, que hagan de la experiencia de conocer Co�aripe y sus alrededores, en un deleite para el esp�ritu, por la belleza de sus paisajes y la magia de su entorno, haciendo de su estad�a una experiencia tan agradable, que su recomendaci�n hable por nosotros y haga de nuestro trabajo una meta cumplida.   
       </p>
            
	<h2>Nuestra Visi�n</h2>
       	<p>Hacer de Corretajes Co�aripe una empresa l�der, no s�lo en el mercado regional, sino en todo el sur del pa�s, ofreciendo a nuestros clientes una administraci�n confiable que les otorgue grandes beneficios, y entregar a nuestros usuarios alternativas en todos los rincones del maravilloso Sur de Chile.</p>
       

          </section>