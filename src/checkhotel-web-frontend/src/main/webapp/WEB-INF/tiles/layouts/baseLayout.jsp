<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no">
    <title><spring:message code="title" text="Corretaje Coñaripe - sin placeholder" /></title>
    
	<link href="<c:url value='/resources/css/conaripe.css'  />" rel="stylesheet" />
	<link href="<c:url value='/resources/css/prettyPhoto.css'/>" rel="stylesheet" />
	<link href="<c:url value='/resources/css/ui.datepicker.css'/>" rel="stylesheet" />
	
	<script src="<c:url value='/resources/js/jquery-1.9.1.min.js' />"></script>
	<script src="<c:url value='/resources/js/jquery-ui.custom.js' />"></script>
	<script src="<c:url value='/resources/js/datepicker-es.js' />"></script>
	<script src="<c:url value='/resources/js/util.js' />"></script>
	
	<!--[if IE]>
    	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- <script src="http://falconmasters.com/demos/menu_responsive/menu.js"></script> -->

	
<script type="text/javascript">
jQuery(function($){
	$.datepicker.regional['es'] = {
		closeText: 'Cerrar',
		prevText: '&#x3c;Ant',
		nextText: 'Sig&#x3e;',
		currentText: 'Hoy',
		monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio',
		'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
		monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun',
		'Jul','Ago','Sep','Oct','Nov','Dic'],
		dayNames: ['Domingo','Lunes','Martes','Mi&eacute;rcoles','Jueves','Viernes','S&aacute;bado'],
		dayNamesShort: ['Dom','Lun','Mar','Mi&eacute;','Juv','Vie','S&aacute;b'],
		dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S&aacute;'],
		weekHeader: 'Sm',
		dateFormat: 'dd-mm-yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
	$.datepicker.setDefaults($.datepicker.regional['es']);
});    
 </script>
	
</head>
  
<body>
        <header id="header">
            <tiles:insertAttribute name="header" />
        </header>
     
<!--         <section id="sidemenu"> -->
<%--             <tiles:insertAttribute name="menu" /> --%>
<!--         </section> -->
        
        <section id="site-content">
            <tiles:insertAttribute name="content" />
        </section>
         
        <footer id="footer">
            <tiles:insertAttribute name="footer" />
        </footer>
</body>
</html>