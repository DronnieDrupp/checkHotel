<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<footer>
	<p>&copy; 2016 Corretajes Co�aripe Ltda. Todos los Derechos Reservados</p>
	<ul>
		<li><a href="${pageContext.request.contextPath}/">Portada</a></li>
		<li><a href="${pageContext.request.contextPath}/aboutUs">Qui�nes Somos</a></li>
		<li><a href="${pageContext.request.contextPath}/rooms">Nuestras Caba�as</a></li>
		<li><a href="${pageContext.request.contextPath}/booking">Mi Reserva</a></li>
		<li><a href="${pageContext.request.contextPath}/contactUs">Contacto</a></li>
	</ul>
</footer>