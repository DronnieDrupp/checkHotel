<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<h1><a href="${pageContext.request.contextPath}/">Corretajes Co�aripe</a></h1>
<div class="menu_bar">
<a class="bt-menu" href="#"><span class="icon-list2"></span>Menu</a>
</div>
  
<nav>
<ul id="menu">
	<li><a href="${pageContext.request.contextPath}/">Portada</a></li>
	<li><a href="${pageContext.request.contextPath}/aboutUs">Qui�nes Somos</a></li>	
	<li><a href="${pageContext.request.contextPath}/palaces">Nuestros Centros</a></li>
	<li><a href="${pageContext.request.contextPath}/bookings">Mi Reserva</a></li>
	<li><a href="${pageContext.request.contextPath}/contactUs">Contacto</a></li>   
</ul>
</nav>

<script src="<c:url value='/resources/js/menu.js' />"></script>
<h2>Reserva tu estad�a en los m�s bellos paisajes del Sur de Chile y disfruta de la naturaleza, con la comodidad que mereces</h2>