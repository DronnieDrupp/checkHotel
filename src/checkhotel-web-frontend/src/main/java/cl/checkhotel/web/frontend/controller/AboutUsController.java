package cl.checkhotel.web.frontend.controller;

import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/aboutUs")
public class AboutUsController {


	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(AboutUsController.class);

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {
		
		String now = (new Date()).toString();
		logger.info("AboutUsController " + now);

		return new ModelAndView("aboutUs/quienesSomos");
	}
}
