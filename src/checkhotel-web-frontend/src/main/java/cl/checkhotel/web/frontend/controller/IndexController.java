package cl.checkhotel.web.frontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import cl.checkhotel.domain.entity.bussines.ObjectSearch;


@Controller
@RequestMapping("/")
public class IndexController {

	private static final String INDEX_PATH = "index";

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {
		
		model.addAttribute("objectSearch", new ObjectSearch());

		return new ModelAndView(INDEX_PATH);
	}
}