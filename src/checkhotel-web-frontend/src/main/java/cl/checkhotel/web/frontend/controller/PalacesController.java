package cl.checkhotel.web.frontend.controller;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cl.checkhotel.domain.entity.bussines.PalaceEntity;
import cl.checkhotel.services.adm.PalaceServices;
import cl.checkhotel.web.frontend.config.AppConfigManager;
import cl.checkhotel.web.util.CrudControllerTemplate;
import cl.checkhotel.web.util.CrudControllerTemplate.ListDataProvider;
import cl.checkhotel.web.util.CrudControllerTemplate.NewInstaceDomainObjectProvider;

@Controller
@RequestMapping("/palaces")
public class PalacesController {
	
	@Autowired
	private PalaceServices palaceServices;

	@Autowired
	private AppConfigManager appConfigManager;

	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(PalacesController.class);

	private CrudControllerTemplate<PalaceEntity, Long> template;

	public PalacesController() {
		template = new CrudControllerTemplate<PalaceEntity, Long>("palace",
				"palaces", new NewInstaceDomainObjectProvider<PalaceEntity>() {

					@Override
					public PalaceEntity getNewInstance() {
						return new PalaceEntity();
					}
				});
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public String list(@PageableDefault(direction = Direction.DESC, sort = { "description" }) Pageable pageable, Model model) {
		
		return template.listAll(pageable, model, (Errors) null,
				new ListDataProvider<PalaceEntity>() {

					@Override
					public Page<PalaceEntity> getData(Pageable pageable) {
						return palaceServices.listPalaceEntities(appConfigManager.getCodClient(), pageable);
					}

				});
	}

}
