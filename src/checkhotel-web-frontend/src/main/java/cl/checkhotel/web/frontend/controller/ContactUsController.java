package cl.checkhotel.web.frontend.controller;

import java.util.Date;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import cl.checkhotel.domain.model.SendEmailModel;
import cl.checkhotel.web.frontend.config.AppConfigManager;
import cl.checkhotel.web.frontend.config.SendMail;

@Controller
@RequestMapping("/contactUs")
public class ContactUsController {

	private static final String CONTACT_US_CONTACTO = "contactUs/contacto";

	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(ContactUsController.class);

	@Autowired
	private SendMail sendMail;

	@Autowired
	private AppConfigManager appConfigManager;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome(Model model) {

		String now = (new Date()).toString();
		logger.info("contactUs " + now);

		model.addAttribute("sendEmailModel", new SendEmailModel());

		return new ModelAndView(CONTACT_US_CONTACTO);
	}

	@RequestMapping(value = "/sendEmail", method = RequestMethod.POST)
	public ModelAndView sedEmail(
			@ModelAttribute("sendEmailModel") SendEmailModel mail) {
		
		String messageResponse = "";
		try {
			
			sendMail.sendMail(createMessage(mail));
			
			messageResponse = "";
			
		} catch (Exception ex) {
			
			messageResponse = "Su Correo no ha sido enviado, pruebe más tarde";
		}

		return new ModelAndView(CONTACT_US_CONTACTO, "msg", messageResponse);

	}

	private SimpleMailMessage createMessage(SendEmailModel mail) {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(mail.getEmailFrom());
		message.setTo(appConfigManager.getEmailClient());
		message.setSubject(mail.getSubject());
		message.setText(mail.getMessage());

		return message;
	}

}
