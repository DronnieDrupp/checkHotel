package cl.checkhotel.web.frontend.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.Valid;
import javax.validation.Validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cl.checkhotel.domain.entity.bussines.BookingEntity;
import cl.checkhotel.domain.entity.bussines.ObjectSearch;
import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.services.adm.CountryServices;
import cl.checkhotel.services.adm.RoomServices;
import cl.checkhotel.services.book.BookingServices;
import cl.checkhotel.web.frontend.config.AppConfigManager;
import cl.checkhotel.web.frontend.config.SendMail;
import cl.checkhotel.web.util.CrudControllerTemplate;
import cl.checkhotel.web.util.CrudControllerTemplate.Executor;
import cl.checkhotel.web.util.CrudControllerTemplate.FindByIdExecutor;
import cl.checkhotel.web.util.CrudControllerTemplate.NewInstaceDomainObjectProvider;

@Controller
@RequestMapping("/bookings")
public class BookingController {

	@Autowired
	private BookingServices bookingServices;

	@Autowired
	private RoomServices roomServices;

	@Autowired
	private CountryServices countryServices;

	@Autowired
	private AppConfigManager appConfigManager;

	@Autowired
	private SendMail sendMail;

	@Autowired
	private Validator validator;

	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

	private CrudControllerTemplate<BookingEntity, Long> template;

	public BookingController() {
		template = new CrudControllerTemplate<BookingEntity, Long>(
				"bookingEntity", "bookings",
				new NewInstaceDomainObjectProvider<BookingEntity>() {

					@Override
					public BookingEntity getNewInstance() {
						return new BookingEntity();
					}
				});
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				new SimpleDateFormat("dd-MM-yyyy"), true, 10));
	}

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("objectSearch", new ObjectSearch());
		
		return template.index(model);
	}

	/**
	 * Muesta la pagina de agregar registro cuando invocas la url /bookings?form
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(params = "form", method = RequestMethod.GET)
	public String createForm(Model model) {

		return template.createForm(model);
	}

	/**
	 * Reacciona a un formulario que su action tenga la url /bookings/add y
	 * metodo PUT para agregar una nueva reserva
	 * 
	 * @param bookingEntity
	 *            nueva reserva
	 * @param result
	 *            errores
	 * @param model
	 *            modelo
	 * @return
	 */
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String create(
			@ModelAttribute("bookingEntity") BookingEntity bookingEntity,
			BindingResult result, Model model) {

		String returnTemplateSave = template.save(bookingEntity, result, model,
				null, new Executor<BookingEntity>() {

					@Override
					public void execute(BookingEntity backingObject) {
						bookingServices.createBookings(backingObject);

					}
				});

		if (StringUtils.startsWithIgnoreCase(returnTemplateSave, "redirect:/")) {

			bookingEntity.setRoomEntity(getRoomById(bookingEntity));
			model.addAttribute("bookingEntity", bookingEntity);

			sendEmail(model, bookingEntity);
			return "redirect:/bookings/" + bookingEntity.getId() + "/voucher";
		}

		return "bookings/add";
	}

	@RequestMapping(value = "/{id}/voucher", method = RequestMethod.GET)
	public String voucher(@PathVariable Long id, Model model) {
		template.show(model, bookingServices.searchIdBooking(id));

		return "bookings/voucher";
	}

	/**
	 * Muestra la pagina de modificacion de una reserva cuando se invoca la url
	 * /bookings/id?form
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", params = "form", method = RequestMethod.GET)
	public String updateForm(@PathVariable Long id, Model model) {

		return template.updateForm(model, bookingServices.searchIdBooking(id));
	}

	/**
	 * Muestra la pagina de modificacion de una reserva cuando se invoca la url
	 * /bookings/id?form
	 * 
	 * @param id
	 * @param model
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "Rooms", method = RequestMethod.POST)
	public String searchRooms(
			@RequestParam String url,
			@Valid @ModelAttribute("objectSearch") ObjectSearch objectSearch,
			@PageableDefault(direction = Direction.DESC, sort = { "description" }) Pageable pageable,
			BindingResult result, Model model) throws ParseException {

		BookingEntity bookingEntity = createBookingBySearch(objectSearch);

		model.addAttribute("rooms", roomServices.listRoomForBooking(
				appConfigManager.getCodClient(), bookingEntity, pageable));

		template.updateForm(model, bookingEntity);

		return "bookings/rooms";

	}

	private BookingEntity createBookingBySearch(ObjectSearch search) {
		BookingEntity newBook = new BookingEntity();

		newBook.setDateFrom(search.getDateFrom());
		newBook.setDateTo(search.getDateTo());
		newBook.setNumberSearch(search.getNumberPerson());

		return newBook;
	}

	@RequestMapping(value = "/listRooms", method = RequestMethod.GET)
	public String viewRooms(Model model) {

		return "bookings/rooms";
	}

	@RequestMapping(value = "add", method = RequestMethod.POST)
	public String createBook(
			@ModelAttribute("booking") BookingEntity bookingEntity,
			BindingResult result, Model model) {

		bookingEntity.setRoomEntity(getRoomById(bookingEntity));
		model.addAttribute("objectSearch", new ObjectSearch());
		model.addAttribute("countries", countryServices.listAll());

		return template.createForm(model, bookingEntity);
	}

	private RoomEntity getRoomById(BookingEntity bookingEntity) {
		return roomServices.roomById(bookingEntity.getRoomEntity().getId());
	}

	/**
	 * Reacciona a un formulario que su action tenga la url /bookings y metodo
	 * POST para agregar una nueva reserva
	 * 
	 * @param bookingEntity
	 * @param result
	 * @param model
	 * @return
	 */
		@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
		public String udpate(@PathVariable("id") final Long id,
				BookingEntity bookingEntity, BindingResult result, Model model) {
	
			bookingEntity.setRoomEntity(getRoomById(bookingEntity));
	
			String tep = template.update(bookingEntity, result, model,
					(RedirectAttributes) null, new Executor<BookingEntity>() {
	
						@Override
						public void execute(BookingEntity backingObject) {
							backingObject.setId(id);
							bookingServices.updateBooking(backingObject);
	
						}
					});
			
			if(0 == result.getErrorCount()){
				
				model.addAttribute("bookingEntity", bookingEntity);
				
				sendEmail(model, bookingEntity);
				
				return "bookings/voucher";
				
			}
			
			
			return tep;
	
		}

	/**
	 * Reacciona a un formulario que tenga la url /bookins y metodo delete
	 * 
	 * @param bookingEntity
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public String delete(@PathVariable Long id, Model model) {

		return template.delete(id, model, new Executor<Long>() {

			@Override
			public void execute(Long id) {
				bookingServices.cancelBooking(id);
			}
		});
	}

	// ojo el parametro BindingResult o Errors se declara siempre despues de
	// model
	@RequestMapping(params = "find", method = RequestMethod.POST)
	public String searchBooking(
			@ModelAttribute("booking") BookingEntity bookingEntity,
			BindingResult result, Model model) {

		return template.find(model, result, bookingEntity,
				new FindByIdExecutor<BookingEntity, Long>() {

					@Override
					public BookingEntity execute(Long id) {
						return bookingServices.searchIdBooking(id);
					}
				});

	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public String view(@PathVariable Long id, Model model) {
		BookingEntity booking = bookingServices.searchIdBooking(id);
		return template.show(model, booking);
	}

	private void sendEmail(Model model, BookingEntity book) {

		String messageResponse = "";
		String cssMsg = "";
		try {

			sendMail.sendMail(createMail(book));

			messageResponse = "Una Copia de su reserva ha sido enviada a su correo";
			cssMsg = "succesMsg";

		} catch (Exception ex) {

			messageResponse = "Su Correo no ha sido enviado, Pongansé en contacto con nosotros";
			cssMsg = "errorMsg";
		}

		model.addAttribute("message", messageResponse);
		model.addAttribute("cssMsg", cssMsg);

	}

	private SimpleMailMessage createMail(BookingEntity book) {
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom(appConfigManager.getEmailClient());
		message.setTo(book.getGuestEntity().getEmail());
		message.setSubject("Confirmación de Reserva");
		message.setText(createMsg(book));

		return message;
	}

	private String createMsg(BookingEntity book) {
		// TODO Auto-generated method stub
		String header = "Querido, " + book.getGuestEntity().getFullName()
				+ ", su número de reserva es: " + book.getId() + ".\r\n";
		String body = "En nuestra cabaña: "
				+ book.getRoomEntity().getDescription() + ", de la fecha: "
				+ book.getDateFrom() + " a la fecha: " + book.getDateTo()
				+ "valor total estadia: " + book.getTotalBook()
				+ ".\r\n Saludos Cordiales de Corrataje Coñaripe.";

		return StringUtils.join(new String[] { header, body }, ' ');
	}

}
