package cl.checkhotel.web.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cl.checkhotel.domain.entity.bussines.RoomEntity;
import cl.checkhotel.services.adm.RoomServices;
import cl.checkhotel.web.util.CrudControllerTemplate;
import cl.checkhotel.web.util.CrudControllerTemplate.ListDataProvider;
import cl.checkhotel.web.util.CrudControllerTemplate.NewInstaceDomainObjectProvider;

@Controller
@RequestMapping("/palace/{palaceId}/rooms")
public class RoomsOfPalaceController {

	// private final static org.slf4j.Logger logger =
	// LoggerFactory.getLogger(RoomController.class);

	@Autowired
	private RoomServices roomServices;

	private CrudControllerTemplate<RoomEntity, Long> template;

	public RoomsOfPalaceController() {
		template = new CrudControllerTemplate<RoomEntity, Long>("room", "rooms",
				new NewInstaceDomainObjectProvider<RoomEntity>() {

					@Override
					public RoomEntity getNewInstance() {
						return new RoomEntity();
					}
				});
	}

	@RequestMapping
	public String list(@PathVariable("palaceId") final Long palaceId,
			@PageableDefault(direction = Direction.DESC, sort = { "description" }) Pageable pageable, Model model) {
		model.addAttribute("palaceId", palaceId);

		return template.listAll(pageable, model, (Errors) null,
				new ListDataProvider<RoomEntity>() {

					@Override
					public Page<RoomEntity> getData(Pageable pageable) {
						return roomServices.listRoomEntities(palaceId, pageable);
					}

				});

//		String returnTemplateList = template.listAll(
//		if (StringUtils.startsWithIgnoreCase(returnTemplateList, "redirect:/")) {
//			returnTemplateList = StringUtils.replace(returnTemplateList, "redirect:", "redirect:/palaces/" + palaceId);
//		}

//		return returnTemplateList;

	}

}
